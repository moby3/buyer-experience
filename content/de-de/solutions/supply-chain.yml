---
  title: Sicherheit der Softwarebereitstellungskette
  description: Schütze deine Softwarebereitstellungskette, bleibe Bedrohungsvektoren einen Schritt voraus und lege Richtlinien fest, um die Einhaltung der Vorschriften zu vereinfachen und schneller sichere Software bereitstellen zu können.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  Integrierte Automatisierung und Richtliniendurchsetzung
        title: Sicherheit der Softwarebereitstellungskette
        subtitle: Schütze deine Softwarebereitstellungskette, bleibe Bedrohungsvektoren einen Schritt voraus und lege Richtlinien fest, um die Einhaltung der Vorschriften zu vereinfachen und schneller sichere Software bereitstellen zu können.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Ultimate kostenlos testen
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Erfahre mehr über die Preise
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/resources/resources_11.jpeg
          image_url_mobile: /nuxt-images/supply-chain-hero.png
          alt: "Bild: Sicherheit der Softwarebereitstellungskette"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: Diese Unternehmen vertrauen uns
        logos:
          - name: Bendigo and Adelaide Bank Logo
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: /customers/bab/
            aria_label: Link zur Bendigo und Adelaide Bank Kunden-Fallstude
          - name: HackerOne Logo
            image: /nuxt-images/logos/hackerone-logo.png
            url: /customers/hackerone/
            aria_label: Link zur HackerOne Kunden-Fallstudie
          - name: new10 Logo
            image: /nuxt-images/logos/new10.svg
            url: /customers/new10/
            aria_label: Link zur new10 Kunden-Fallstudie
          - name: The Zebra Logo
            image: /nuxt-images/logos/zebra.svg
            url: /customers/thezebra/
            aria_label: Link zur The Zebra Kunden-Fallstudie
          - name: Chorus Logo
            image: /nuxt-images/logos/chorus.svg
            url: /customers/chorus/
            aria_label: Link zur Chorus Kunden-Fallstudie
          - name: Hilti Logo
            image: /nuxt-images/logos/hilti_logo.svg
            url: /customers/hilti/
            aria_label: Link zur Hilti Kunden-Fallstudie
    - name: 'side-navigation-variant'
      links:
        - title: Übersicht
          href: '#overview'
        - title: Leistungen
          href: '#capabilities'
        - title: Kundenstimmen
          href: '#customers'
        - title: Preise
          href: '#pricing'
        - title: Ressourcen
          href: '#resources'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Schütze deine gesamte Softwarebereitstellungskette
                is_accordion: false
                items:
                  - icon:
                      name: continuous-integration
                      alt: "Symbol: kontinuierliche Integration"
                      variant: marketing
                    header: Schütze den Entwicklungslebenszyklus deiner Software
                    text: Sichere mehrere Angriffsflächen, einschließlich Code, Build, Abhängigkeiten und Release-Artefakte
                    link_text: Erfahre mehr über DevSecOps
                    link_url: /solutions/security-compliance/
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: devsecops
                      alt: "Symbol: DevSecOps"
                      variant: marketing
                    header: Einhaltung von Konformitätsanforderungen
                    text: Einfacher Zugriff auf Prüf- und Governance-Berichte
                    link_text: Gründe, die für GitLab sprechen
                    link_url: /why-gitlab/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: release
                      alt: "Symbol: Schild mit Häkchen"
                      variant: marketing
                    header: Implementiere Schutzmaßnahmen
                    text: Kontrolliere den Zugriff und implementiere Richtlinien
                    link_text: Erfahre mehr über unseren Plattform-Ansatz
                    link_url: /solutions/devops-platform/
            - name: 'solutions-video-feature'
              data:
                video:
                  url: 'https://player.vimeo.com/video/762685637?h=f96e969756'
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: Code, Build, Release. Sicher.
                sub_description: ''
                white_bg: true
                markdown: true
                sub_image: /nuxt-images/solutions/supply-chain.png
                solutions:
                  - title: Etablierung von Zero Trust
                    description: |
                      Das Identitäts- und Zugriffsmanagement (IAM, Identity and Access Management) zählt zu den größten Angriffsvektoren in der Softwarebereitstellungskette. Schütze den Zugriff mit GitLab, indem du alle menschlichen und maschinellen Identitäten, die in deiner Umgebung tätig sind, authentifizierst, autorisierst und kontinuierlich validierst.
                      *  Implementiere eine granulare [Zugriffskontrolle](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html), einschließlich [2-Faktor-Authentifizierung](https://docs.gitlab.com/ee/security/two_factor_authentication.html).
                      *  Erstelle [Token-Ablaufrichtlinien](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).
                      *  Lege [Richtlinien](https://docs.gitlab.com/ee/administration/compliance.html#policy-management) gemäß betrieblichen Regeln oder gesetzlichen Vorschriften fest.
                      *  Erstelle umfassende [Prüf- und Governance-Berichte](https://docs.gitlab.com/ee/administration/audit_reports.html) zur Sicherstellung der Konformität.
                      *  Erzwinge [Zwei-Personen-Genehmigungen](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html) als zusätzliche Schutzvorkehrung.
                  - title: Schütze deinen Quellcode
                    description: |
                      Stelle die Sicherheit und Integrität deines Quellcodes sicher, indem du verwaltest, wer Zugriff auf den Code hat und wie Änderungen am Code überprüft und zusammengeführt werden.

                      *  Richte eine Versionskontrolle, [Codeverlauf](https://docs.gitlab.com/ee/user/project/repository/git_history.html?_gl=1*1ngzpgw*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NDUzNDg3My4xMjkuMS4xNjY0NTM4MjQ3LjAuMC4w) und [Zugriffskontrolle](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html) für deinen Quellcode ein.
                      *  Verwende automatisierte Tests für die [Codequalität](https://docs.gitlab.com/ee/ci/testing/code_quality.html), um die Auswirkungen von Änderungen auf die Leistung zu analysieren.
                      *  Erzwinge Überprüfungs- und [Genehmigungsregeln](https://docs.gitlab.com/ee/ci/testing/code_quality.html), um zu steuern, was in die Produktion einfließt.
                      *  Führe [automatisierte Sicherheitsscans](https://docs.gitlab.com/ee/user/application_security/) durch, um Schwachstellen zu erfassen, bevor dein Code zusammengeführt wird.
                      *  Stelle mittels automatisierter [Geheimniserkennung](https://docs.gitlab.com/ee/user/application_security/secret_detection/) sicher, dass dein Quellcode keine Passwörter und vertrauliche Informationen enthält.
                      *  Implementiere [signierte Commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/), um Identitätsdiebstahl im Hinblick auf Entwickler(innen) zu verhindern.
                  - title: Schütze Abhängigkeiten
                    description: |
                      Stelle sicher, dass alle in deinen Projekten verwendeten Open-Source-Abhängigkeiten keine bekannten Sicherheitslücken enthalten, dass sie aus einer vertrauenswürdigen Quelle stammen und dass sie nicht manipuliert wurden.

                      *  Erstelle eine automatisierte [Leistungsbeschreibung der Software](https://docs.gitlab.com/ee/user/application_security/dependency_list/), um die Abhängigkeiten deiner Projekte zu identifizieren.
                      *  Erkenne mittels einer automatisierten [Analyse der Softwarezusammensetzung](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) automatisch Schwachstellen in jeder verwendeten Software mit Abhängigkeiten.
                      *  Prüfe die [Lizenzkonformität](https://docs.gitlab.com/ee/user/compliance/license_compliance/), um sicherzustellen, dass dein Projekt Software verwendet, für die dein Unternehmen lizensiert ist.
                  - title: Schütze Build-Umgebungen
                    description: |
                      Verhindere, dass bösartige Akteure schädlichen Code in den Build-Prozess injizieren und die Kontrolle über die von der Pipeline erstellten Software oder den Zugriff auf in der Pipeline verwendete Geheimnisse erlangen.

                      *  [Isoliere deine Build-Umgebung](https://docs.gitlab.com/runner/security/?_gl=1*1d95r9z*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NDUzNDg3My4xMjkuMS4xNjY0NTM4MDA2LjAuMC4w), um unbefugten Zugriff oder die Ausführung von bösartigem Code zu verhindern.
                      *  Pflege [Release-Beweise](https://docs.gitlab.com/ee/user/project/releases/#release-evidence) für alles, was in dem Release enthalten ist.
                      *  Stelle sicher, dass deine Build-Artefakte nicht durch [Build-Artefakt-Attestation](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#artifact-attestation) kompromittiert werden.
                  - title: Schütze Release-Artefakte
                    description: |
                      Verhindere, dass Angreifer Schwachstellen im Design oder in den Konfigurationen einer Anwendung ausnutzen, um private Daten zu stehlen, unbefugten Zugriff auf Konten zu erhalten oder sich als legitime Benutzer auszugeben.

                      *  Richte eine [sichere Verbindung](https://about.gitlab.com/blog/2022/01/07/gitops-with-gitlab-using-ci-cd/#meet-the-cicd-tunnel) zu deinem Cluster ein, um deine Release-Artefakte bereitzustellen.
                      *  Identifiziere [Sicherheitslücken in laufenden Anwendungen](https://docs.gitlab.com/ee/user/application_security/dast/), bevor du sie bereitstellst.
                      *  Stelle sicher, dass deine [API-Schnittstellen](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/) deine laufende Anwendung nicht gefährden.
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                align: left
                header: |
                  Mit dem Vertrauen von Unternehmen.
                  <br />
                  Von Entwickler(inne)n geliebt.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
                      alt: Bending and Adelaide Bank Logo
                    quote: Wir haben jetzt eine Lösung, die stets innovativ ist und zu unserem Ziel der digitalen Transformation passt.
                    author: Caio Trevisan, Head of Devops Enablement, Bendigo and Adelaide Bank
                    ga_carousel: bending and adelaide bank testimonial
                    url: /customers/bab/
                  - title_img:
                      url: /nuxt-images/logos/new10.svg
                      alt: New10 Logo
                    quote: GitLab hilft uns wirklich enorm bei unserer sehr modernen Architektur, denn es unterstützt Kubernetes, es unterstützt serverless und es unterstützt coole Sicherheitsfunktionen wie DAST und SAST. Dank GitLab haben wir eine wirklich hochmoderne Architektur.
                    author: Kirill Kolyaskin, CTO
                    ga_carousel: New10 testimonial
                    url: /customers/new10/
                  - title_img:
                      url: /nuxt-images/logos/zebra.svg
                      alt: The Zebra Logo
                    quote: Der größte Vorteil (von GitLab) ist, dass es den Entwicklungsteams ermöglicht, eine größere Rolle innerhalb des Bereitstellungsprozesses zu spielen. Früher kannten nur wenige Leute sich wirklich gut aus. Aber heute weiß so ziemlich die gesamte Entwicklungsabteilung, wie die CI-Pipeline funktioniert, und alle können damit arbeiten, neue Services hinzufügen und Neues in die Produktion einbringen, ohne dass die Infrastruktur einen Engpass darstellt.
                    author: Dan Bereczki, Sr. Software Manager, The Zebra
                    url: /customers/thezebra/
                    ga_carousel: the zebra testimonial
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: Hilti Logo
                    quote: GitLab wird wie eine Suite gebündelt und mit einem sehr ausgeklügelten Installer ausgeliefert. Und dann funktioniert es wie von Zauberhand. Das ist sehr schön, wenn man ein Unternehmen ist, das einfach nur alles zum Laufen bringen möchte.
                    author: Daniel Widerin, Head of Software Delivery, HILTI
                    url: /customers/hilti/
                    ga_carousel: hilti testimonial
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: Welcher Tarif ist der richtige für dich?
                cta:
                  url: /pricing/
                  text: Erfahre mehr über die Preise
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: Preise
                tiers:
                  - id: free
                    title: Free
                    items:
                      - Statische Anwendungssicherheitstests (SAST) und Geheimniserkennung
                      - Funde in JSON-Datei
                    link:
                      href: /pricing/
                      text: Erste Schritte
                      data_ga_name: pricing
                      data_ga_location: free tier
                      aria_label: Kostenloser Tarif
                  - id: premium
                    title: Premium
                    items:
                      - Statische Anwendungssicherheitstests (SAST) und Geheimniserkennung
                      - Funde in JSON-Datei
                      - MR-Genehmigungen und häufigere Kontrollen
                    link:
                      href: /pricing/
                      text: Mehr erfahren
                      data_ga_name: pricing
                      data_ga_location: premium tier
                      aria_label: Premium Tarif
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Alle Funktionen von Premium und zusätzlich
                      - Umfassende Sicherheitsscanner umfassen SAST, DAST, Geheimnisse, Abhängigkeiten, Container, IaC, APIs, Cluster-Images und Fuzz-Tests
                      - Umsetzbare Ergebnisse innerhalb der MR-Pipeline
                      - Konformitäts-Pipelines
                      - Sicherheits- und Konformitäts-Dashboards
                      - Vieles mehr
                    link:
                      href: /pricing/
                      text: Mehr erfahren
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: Ultimate Tarif
                    cta:
                      href: /free-trial/
                      text: Ultimate kostenlos testen
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: Ultimate Tarif
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            data:
              title: Zugehörige Ressourcen
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: "Symbol: Video"
                  event_type: "Video"
                  header: Verschieben der Sicherheit nach links – Überblick über DevSecOps
                  link_text: "Jetzt ansehen"
                  image: "/nuxt-images/solutions/dev-sec-ops/shifting-security-left.jpeg"
                  href: "https://www.youtube.com/embed/XnYstHObqlA"
                  data_ga_name: Shifting Security Left - DevSecOps Overview
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: "Symbol: Video"
                  event_type: "Video"
                  header: Schwachstellenverwaltung und Aufgabentrennung mit GitLab
                  link_text: "Jetzt ansehen"
                  image: /nuxt-images/solutions/dev-sec-ops/managing-vulnerabilities.jpeg
                  href: https://www.youtube.com/embed/J5Frv7FZtnI
                  data_ga_name: Managing Vulnerabilities and Enabling Separation of Duties with GitLab
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: "Symbol: Video"
                  event_type: "Video"
                  header: Veröffentlichung von GitLab 15 – Neue Sicherheitsfunktionen
                  link_text: "Jetzt ansehen"
                  image: "/nuxt-images/solutions/dev-sec-ops/gitlab-15-release.jpeg"
                  href: https://www.youtube.com/embed/BasGVNvOFGo
                  data_ga_name: GitLab 15 Release - New Security Features
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: "Symbol: Video"
                  event_type: "Video"
                  header: SBOM und Zertifizierung
                  link_text: "Jetzt ansehen"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: "Symbol: E-Book"
                  event_type: "Book"
                  header: "Leitfaden für die Sicherheit der Softwarebereitstellungskette"
                  link_text: "Mehr erfahren"
                  image: "/nuxt-images/resources/resources_13.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: "Symbol: E-Book"
                  event_type: "Book"
                  header: " GitLab DevSecOps-Umfrage"
                  link_text: "Mehr erfahren"
                  image: "/nuxt-images/resources/resources_1.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: " GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: "Symbol: Blog"
                  event_type: "Blog"
                  header: "Ultimativer Leitfaden zur Sicherheit der Softwarebereitstellungskette "
                  link_text: "Mehr erfahren"
                  href: https://about.gitlab.com/blog/2022/08/30/the-ultimate-guide-to-software-supply-chain-security/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "Ultimate guide to software supply chain security "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: "Symbol: Blog"
                  event_type: "Blog"
                  header: "NIST-Framework-Konformität mit GitLab "
                  link_text: "Mehr erfahren"
                  href: https://about.gitlab.com/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/
                  image: "/nuxt-images/resources/resources_7.jpeg"
                  data_ga_name: "Comply to NIST framework with GitLab "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: "Symbol: Blog"
                  event_type: "Blog"
                  header: "Elite-Teamstrategien zum Schutz der Softwarebereitstellungskette "
                  link_text: "Mehr erfahren"
                  href: https://about.gitlab.com/blog/2022/01/06/elite-team-strategies-to-secure-software-supply-chains/
                  image: "/nuxt-images/resources/resources_13.jpeg"
                  data_ga_name: "Elite team strategies to secure the software supply chain "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: "Symbol: Blog"
                  event_type: "Blog"
                  header: "Absicherung der Softwarebereitstellungskette durch automatisierte Zertifizierung"
                  link_text: "Mehr erfahren"
                  href: https://about.gitlab.com/blog/2022/08/10/securing-the-software-supply-chain-through-automated-attestation/
                  image: "/nuxt-images/resources/resources_4.jpeg"
                  data_ga_name: "Securing the software supply chain through automated attestation"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: "Symbol: Bericht"
                    variant: marketing
                  event_type: "Analystenbericht"
                  header: "GitLab ist ein Challenger im Gartner Magic Quadrant 2022"
                  link_text: "Mehr erfahren"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: solutions-cards
      data:
        title: Erreiche mehr mit GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Erkunde weitere Lösungen
          data_ga_name: solutions explore more
          data_ga_location: body
        cards:
          - title: DevSecOps
            description: GitLab versetzt deine Teams in die Lage, Geschwindigkeit und Sicherheit in Einklang zu bringen, indem es die Softwarebereitstellung automatisiert und deine Softwarebereitstellungskette durchgängig schützt.
            icon:
              name: devsecops
              alt: "Symbol: DevSecOps"
              variant: marketing
            href: /solutions/security-compliance/
            cta: Mehr erfahren
            data_ga_name: devsecops learn more
            data_ga_location: body
          - title: Kontinuierliche Software-Konformität
            description: Integriere Sicherheit in deinen DevOps-Lebenszyklus – mit GitLab ist das ganz einfach.
            icon:
              name: build
              alt: "Symbol: Build"
              variant: marketing
            href: /solutions/continuous-software-compliance/
            cta: Mehr erfahren
            data_ga_name: continuous software compliance learn more
            data_ga_location: body
          - title: Kontinuierliche Integration und Bereitstellung
            description: Wiederholbare und bedarfsgerechte Softwarebereitstellung
            icon:
              name: continuous-delivery
              alt: Kontinuierliche Bereitstellung
              variant: marketing
            href: /solutions/continuous-integration/
            cta: Mehr erfahren
            data_ga_name: siemens learn more
            data_ga_location: body

