---
  title: GitLab DevSecOps World Tour
  og_title: GitLab DevSecOps World Tour
  description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation.
  og_description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation.
  twitter_description: Get connected to the ideas, technologies, and people that are driving DevSecOps transformation.
  og_image: /nuxt-images/events/world-tour/world-tour-social.png
  twitter_image: /nuxt-images/events/world-tour/world-tour-social.png
  hero:
    header: DevSecOps World Tour
    subtitle: |
      Get connected to the ideas, technologies, and people that are driving DevSecOps transformation. This event is for technology and business leaders, fueling the transformation and growth of companies with software. GitLab DevSecOps will enable you with best practices to fuel your software innovation and strengthen your security and compliance.
    image:
      src: /nuxt-images/events/world-tour/hero-image.png
      alt: commit illustration
    button:
      text: Find a World Tour near you
      href: '#cities'
      variant: secondary
      icon:
        name: arrow-down
        variant: product
        size: sm
  regions:
    - name: Americas
      cities:
        - name: Atlanta
          state: GA
          day: 15
          month: June
          href: /events/devsecops-world-tour/atlanta
          image:
            src: /nuxt-images/events/world-tour/cities/atlanta.png
            alt: atlanta sketch
        - name: Chicago
          state: IL
          day: 26
          month: July
          href: /events/devsecops-world-tour/chicago
          image:
            src: /nuxt-images/events/world-tour/cities/chicago.png
            alt: chicago sketch
        - name: Mountain View
          state: CA
          day: 19
          month: September
          href: "/events/devsecops-world-tour/mountain-view"
          image:
            src: /nuxt-images/events/world-tour/cities/mountain-view.png
            alt: mountain view sketch
        - name: Irvine
          state: CA
          day: 21
          month: September
          href: "/events/devsecops-world-tour/irvine"
          image:
            src: /nuxt-images/events/world-tour/cities/irvine.png
            alt: irvine sketch
        - name: Dallas
          state: TX
          day: 5
          month: October
          href: /events/devsecops-world-tour/dallas
          image:
            src: /nuxt-images/events/world-tour/cities/dallas.png
            alt: dallas sketch
        - name: New York
          state: NY
          day: 12
          month: October
          href: "/events/devsecops-world-tour/new-york"
          image:
            src: /nuxt-images/events/world-tour/cities/new-york.png
            alt: new york sketch
        - name: Washington
          state: D.C.
          day: 25
          month: October
          href: "/events/devsecops-world-tour/washington-dc"
          image:
            src: /nuxt-images/events/world-tour/cities/washington.png
            alt: washington sketch
          additionalLogo:
            src: /nuxt-images/events/world-tour/CPE_Eligible-All_Black.png
            alt: cpe eligible badge
    - name: Europe
      cities:
        - name: London
          state: UK
          day: 12
          month: September
          href: "/events/devsecops-world-tour/london/"
          image:
            src: /nuxt-images/events/world-tour/cities/london.png
            alt: london sketch
        - name: Paris
          state: FR
          day: 17
          month: October
          href: "/events/devsecops-world-tour/paris/"
          image:
            src: /nuxt-images/events/world-tour/cities/paris.png
            alt: paris sketch
        - name: Berlin
          state: DE
          day: 19
          month: October
          href: "/events/devsecops-world-tour/berlin/"
          image:
            src: /nuxt-images/events/world-tour/cities/berlin.png
            alt: berlin sketch
    - name: Asia-Pacific
      cities:
      - name: Tokyo
        state: JP
        day: 8-9
        month: November (Virtual)
        href: "#registration-form"
        image:
          src: /nuxt-images/events/world-tour/cities/tokyo.png
          alt: tokyo sketch
      - name: Melbourne
        state: AU
        day: 17
        month: August
        href: "/events/devsecops-world-tour/melbourne/"
        image:
          src: /nuxt-images/events/world-tour/cities/melbourne.png
          alt: melbourne sketch
  form:
    header: Get updated when DevSecOps World Tour comes to a city near you
    form_id: 3586
  faq:
    header: Frequently Asked Questions
    data_inbound_analytics: pricing-leap-target
    background: true
    groups:
      - header: "DevSecOps World Tour"
        questions:
          - question: What is DevSecOps World Tour?
            answer: >-
              The GitLab DevSecOps World Tour is a free event that brings developers, security and operations professionals, and technology leaders together to discover how GitLab can help teams take software development to new frontiers. With 12 stops around the globe, DevSecOps World Tour brings all the latest trends and innovations in the DevSecOps space to a city near you.

              At every stop we connect you to the ideas, technologies, and people that are driving business and software transformation. Join the GitLab team and your fellow technology practitioners and leaders to learn about the latest GitLab capabilities and how organizations are using GitLab to build more secure software, faster. You’ll come back with practical information you can apply to your business immediately.

          - question: Who should attend?
            answer: >-
              DevSecOps World Tour is for anyone who is interested in technology transformation — from CTOs, CISOs, VPs, DevOps engineers, and security pros to SREs, and developers.
          - question: How much does it cost?
            answer: |
              The event is free to attend.
          - question: Will meals be provided?
            answer: >-
              Yes, meals will be provided.
          - question: How will I receive my registration confirmation? Can I modify or transfer my registration prior to the event? If so, what is the cancellation policy?
            answer: >-
              You will receive a registration confirmation email from [events@gitlab.com](mailto:events@gitlab.com). You may modify/transfer/cancel up until two days before the event by emailing [events@gitlab.com](mailto:events@gitlab.com).
          - question: How can I contact the DevSecOps World Tour team if I have more questions or want to provide feedback?
            answer: >-
              Please reach out to [events@gitlab.com](mailto:events@gitlab.com).
  conduct_banner:
    header: Code of conduct
    text: GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](/company/culture/ecoc/) to ensure DevSecOps World Tour is a friendly, inclusive, and comfortable environment for all participants.
