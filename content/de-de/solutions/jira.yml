---
  title: GitLab mit Jira
  description: Automatisiere deine Arbeit von GitLab zu Jira
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab mit Jira
        subtitle: Automatisiere deine Arbeit von GitLab zu Jira
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: /learn/#agile_management
          text: Jetzt lernen
          data_ga_name: start learning
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Bild: GitLab mit Jira"
    - name: featured-media
      data:
        column_size: 4
        media:
          - title: GitLab <-> Jira-Integration
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Sobald du dein GitLab-Projekt in deine Jira-Instanz integriert hast, kannst du Aktivitäten zwischen dem GitLab-Projekt und jedem deiner Projekte in Jira erkennen und abgleichen.
            icon:
              name: kanban
              alt: kanban
              variant: marketing
          - title: Integration in das Jira Dev-Panel
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              Ergänzend zu unserer bestehenden Jira-Projektintegration kannst du jetzt GitLab-Projekte in das Jira-Entwicklungspanel integrieren.
            icon:
              name: computer-test
              alt: Integration in das Jira Dev-Panel
              variant: marketing
          - title: Migration von Jira zu GitLab
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              GitLab bietet ein robustes Projektmanagement-Tool, das deine agile Planung in einer Plattform vereint, die dein SCM, CI/CD, deine Sicherheit und vieles mehr beinhaltet!
            icon:
              name: scale
              alt: Integration in das Jira Dev-Panel
              variant: marketing
    - name: featured-media
      data:
        header: So funktioniert die GitLab-Jira-Integration
        column_size: 4
        media:
          - title: Grundlegende GitLab <-> Jira-Integration
            aos_animation: zoom-in-up
            aos_duration: 500
            text: |
              * Erwähne eine Jira-Ticket-ID in einer Commit-Nachricht oder in einem MR (Merge Request)
               
              * Erwähne, dass ein Commit oder MR ein bestimmtes Jira-Ticket löst oder schließt
               
              * Zeige Jira-Tickets direkt in GitLab an
            image:
              url: /nuxt-images/enterprise/source-code.jpg
              alt: "GitLab <-> Jira"
          - title: Integration in das Jira Dev-Panel
            aos_animation: zoom-in-up
            aos_duration: 1000
            text: |
              * Greife ganz einfach direkt von einem Jira-Ticket auf zugehörige GitLab Merge Requests, Branches und Commits zu
               
              * Funktioniert mit selbstverwaltetem GitLab oder GitLab.com, das mit Jira integriert ist und von deiner Jira-Cloud gehostet wird
               
              * Verbindet alle GitLab-Projekte innerhalb einer Gruppe auf oberster Ebene oder eines persönlichen Namespaces mit Projekten in der Jira-Instanz
            image:
              url: /nuxt-images/enterprise/ci-cd.jpg
              alt: "Panel-Integration"
          - title: Migration von Jira zu GitLab
            aos_animation: zoom-in-up
            aos_duration: 1500
            text: |
              * Importiere deine Jira-Tickets auf GitLab.com oder in deine selbstverwaltete GitLab-Instanz
               
              * Importiere Titel, Beschreibung und Labels direkt
               
              * Ordne Jira-Benutzer(innen) GitLab-Projektmitgliedern zu
            image:
              url: /nuxt-images/enterprise/agile2.jpg
              alt: "Migration von GitLab zu Jira"
    - name: featured-media
      data:
        header: Anleitungsvideos
        text: |
          Die folgende Liste enthält Ressourcen zur GitLab-Jira-Integration, die wir erstellt haben, um dir das Verständnis der Implementierung zu erleichtern.
        column_size: 4
        media:
          - title: Grundlegende GitLab-Jira-Integration
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Du kannst Inhalte und Prozesse von Jira zu GitLab-Tickets migrieren, aber du kannst auch weiterhin Jira verwenden und es zusammen mit GitLab nutzen.
            link:
              text: Lesen
              href: https://docs.gitlab.com/ee/integration/jira/
              data_ga_name: Gitlab-Jira integration
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/fWvwkx5_00E?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: GitLab-Jira Development Panel
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              Ergänzend zu unserer bestehenden Jira-Projektintegration kannst du GitLab-Projekte in das Jira-Entwicklungspanel integrieren.
            link:
              text: Lesen
              href: https://docs.gitlab.com/ee/integration/jira/
              data_ga_name: Gitlab-Jira development panel
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/VjVTOmMl85M?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Importiere deine Jira-Projekt-Tickets in GitLab
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              Mit dem Jira-Importer von GitLab kannst du deine Jira-Tickets auf GitLab.com oder in deine selbstverwaltete GitLab-Instanz importieren.
            link:
              text: Lesen
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: Import jira issue to GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/cixs4NJB3H8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Jira-Ticketliste in GitLab anzeigen
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Für Unternehmen, die Jira als primäres Tool zur Arbeitsverfolgung nutzen, kann es eine Herausforderung sein, mit mehreren Systemen zu arbeiten und eine einzige Quelle der Wahrheit zu bewahren.
            link:
              text: Lesen
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: View jira issue list in GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/_yDcD_jzSjs?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Ordne Jira-Benutzer(innen) GitLab-Benutzer(innen) zu, wenn du Tickets importierst
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              Wenn du Tickets von Jira nach GitLab importierst, kannst du jetzt Jira-Benutzer(innen) GitLab-Projektmitgliedern zuordnen, bevor du den Import ausführst. So kann der Importer die richtigen Reporter und Verantwortlichen für die Tickets setzen, die du nach GitLab verschiebst.
            link:
              text: Lesen
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: Map jira users to GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/cixs4NJB3H8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: GitLab-Projektmanagement-Roadmap
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              Wir arbeiten ständig daran, die Integration von GitLab in Jira zu verbessern, daher freuen wir uns über Feedback und prüfen, was in den nächsten Versionen kommen wird.
            link:
              text: Lesen
              href: https://gitlab.com/groups/gitlab-org/-/epics/2738/
              data_ga_name: GitLab project management roadmap
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/bT60rJEoWhw?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: logo-links
      data:
        header: Open-Source-Partner
        column_size: 2
        logos:
          - logo_url: /nuxt-images/enterprise/logo-dish.svg
            logo_alt: Dish
            aos_animation: zoom-in
            aos_duration: 200
          - logo_url: /nuxt-images/enterprise/logo-expedia.svg
            logo_alt: Expedia
            aos_animation: zoom-in
            aos_duration: 400
          - logo_url: /nuxt-images/enterprise/logo-goldman-sachs.svg
            logo_alt: Goldman Sachs
            aos_animation: zoom-in
            aos_duration: 600
          - logo_url: /nuxt-images/enterprise/logo-nasdaq.svg
            logo_alt: Nasdaq
            aos_animation: zoom-in
            aos_duration: 800
          - logo_url: /nuxt-images/enterprise/logo-uber.svg
            logo_alt: Uber
            aos_animation: zoom-in
            aos_duration: 1000
          - logo_url: /nuxt-images/enterprise/logo-verizon.svg
            logo_alt: Verizon
            aos_animation: zoom-in
            aos_duration: 1200
