---
  title: "London World Tour"
  og_title: London World Tour
  description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  twitter_description: Join us and commit to a day of DevOps inspiration and innovation at GitLab Commit. Now accepting talk submissions!
  og_image: /nuxt-images/events/world-tour/cities/London illustration.png
  twitter_image: /nuxt-images/events/world-tour/cities/London illustration.png
  time_zone: British Summer Time (BST)
  registrationClosed: true
  hero:
    breadcrumbs:
      - title: DevSecOps World Tour
        href: /events/devsecops-world-tour/
      - title: London
    header: London
    subtitle: September 12, 2023
    location: |
      LABS House
      15-19 Bloomsbury Way
      London WC1A 2TH, United Kingdom
    image:
      src: /nuxt-images/events/world-tour/cities/London illustration.png
    button:
      text: Register for London
      href: '#registration-form'
      variant: secondary
      icon:
        name: arrow-down
        variant: product
        size: sm
  agenda:
    - time: 8:30 am
      name: Registration & Breakfast
      speakers:

    - time: 9:30 am
      name: |
        Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps
      speakers:
        - name: Ashley Kramer
          title: Chief Marketing and Strategy Officer
          company: GitLab
          image:
            src:  /nuxt-images/events/world-tour/speakers/ashley-kramer.jpg
          biography:
            text: |
              Ashley Kramer is GitLab’s Chief Marketing and Strategy Officer. Ashley leverages her leadership experience in marketing, product, and technology to position GitLab as the leading DevSecOps platform. She also leads the strategy for product-led growth and code contribution to the GitLab platform.

              Prior to GitLab, Ashley was CPO and CMO of Sisense and has held several leadership roles, including SVP of Product at Alteryx and Head of Cloud at Tableau, as well as marketing, product, and engineering leadership roles at Amazon, Oracle, and NASA.
      description:
        title: 'Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps'
        text: |
          Join us at the GitLab DevSecOps World Tour in London where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape. Learn how Artificial Intelligence (AI) is set to impact all teams across the SDLC and how we envision organizations confidently securing their software supply chain while staying laser focused on delivering business value.

    - time: 10:00 am
      name: Next up for The DevSecOps Platform + Product Demo
      speakers:
        - name: David DeSanto
          title: Chief Product Officer
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/David DeSanto.png
          biography:
            text: David DeSanto is GitLab’s Chief Product Officer where he focuses on delivering a stellar product experience to GitLab’s users from startups to global enterprises. David defines GitLab’s product strategy and vision as well as leads the Product division who executes on this vision. David has led the launches of capabilities that are critical to customer success, including expanding GitLab’s security and compliance capabilities, improving enterprise agile planning capabilities, expanding Value Stream Analytics capabilities, introducing AI assisted workflows, and launching GitLab Dedicated.
        - name: Fabian Zimmer
          title: Director, Product Management - SaaS Platforms
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Fabian Zimmer.jpg
          biography:
            text: Fabian has a background in Bioinformatics where he analyzed large amounts of genomic and transcriptomic data. He is passionate about building products that solve interesting problems and enjoys working in a fast-paced environment. At GitLab, Fabian is responsible for GitLab's SaaS Platforms section, including GitLab.com and GitLab Dedicated. The SaaS Platforms section aims to create tools, services, and frameworks that make building and deploying GitLab a delightful experience on any hardware at any scale.

      description:
          text: |
            Teams are facing more pressure than ever to deliver software faster. Software development processes are undergoing transformative changes too. Join us to hear about our vision for the GitLab DevSecOps platform, take a deep dive into our latest innovations geared towards helping you build secure, efficient and reliable software and learn how you can keep security at the forefront of your growth.
    - time: 10:45 am
      name: Break
    - time: 11:00 am
      name: |
         In discussion with Paul Kerrison and Jan Claeyssens, Dunelm
      description:
        text: Join Dunelm, to learn how their technology team empowers the business by delivering delightful customer and developer experiences. Find out how they digitally transformed, innovated, and delivered new, cutting-edge services to customers and stakeholders. Their focus on delivering stellar experiences has given them many accolades, and now you get the opportunity to understand how the team at Dunelm achieved this.
      speakers:
        - name: Grigoriy Shlyapinkov
          title: Strategic Customer Success Manager
          company: GitLab
          biography:
            text: |
              Grigoriy Shlyapinkov is a Strategic Customer Success Manager professional with a passion for security and over 10 years of continuously delivering value to customers throughout their journey in the SAS industry.

              Leveraging his extensive background to orchestrate delivery of  positive customer outcome, he proactively manages customer experience. Grigoriy energetically helps to align people, processes, and systems to drive customer-centric operations. He has fun travelling around the world and has been to more than 75 countries. He also loves theater and sports.
          image:
            src: /nuxt-images/events/world-tour/speakers/Grigoriy Shlyapinkov.jpeg

        - name: Paul Kerrison
          title: Director of Engineering and Architecture
          company: Dunelm
          biography:
            text: Paul Kerrison is an avid technologist with over 20 years’ experience in software development and architecture, most recently leading award-winning technology teams for Dunelm and Travis Perkins. At Dunelm, he supports the Engineering, Quality and Architecture chapters across multiple domains. He has a keen interest in the application of modern technologies and approaches, particularly with emerging tech and is a strong proponent of the devops mindset.  He values innovative solutions to real problems, delivered in an iterative way that maximises learning, minimises waste and ensures successful outcomes.
          image:
            src: /nuxt-images/events/world-tour/speakers/Paul Kerrison.png

        - name: Jan Claeyssens
          title: Principal DevSecOps Engineer
          company: Dunelm
          biography:
            text: |
              Jan is a security professional who uses an integrative approach to securing businesses based upon proven frameworks, principles and agile ways of working. His focus is on driving a security-first culture, leveraging automation and modern technologies to enable continuous delivery while minimising risk. His leadership skills enable him to mentor and guide teams towards implementing secure development practices, while his strategic thinking and problem-solving abilities enable him to design and deliver secure, scalable solutions for complex projects.
          image:
            src: /nuxt-images/events/world-tour/speakers/Jan Claeyssens.jpg

    - time: 11:45 pm
      name: Lunch & Networking
    - time: 01:00 pm
      name: |
        **Workshop:** Value Streams Assessment
      speakers:
        - name: Jonathan Fullam
          title: VP of Global Solutions Architecture
          company: GitLab
          biography:
            text: Currently, Jonathan leads a global team of value delivery and technical experts as Vice President of Solutions Architecture at GitLab, focusing on helping enterprises drive their business-oriented outcomes by realizing new capabilities and efficiencies within their operating model, technology strategy, and software delivery and management processes.
          image:
            src: /nuxt-images/events/world-tour/speakers/Jonathan Fullam.jpeg

        - name: Simon Mansfield
          title: Senior Manager, Solutions Architecture
          company: GitLab
          biography:
            text: |
              Simon is a seasoned DevOps and value stream expert with over 17 years in the tech industry. As the Senior Manager of Solutions Architecture at GitLab, he specializes in optimizing software development and delivery processes through cutting-edge DevOps practices. A sought-after speaker, Simon frequently shares his insights on DevOps, automation, and CI/CD pipelines at international conferences. Connect with him on LinkedIn and Twitter for his latest thoughts on the future of software development.
          image:
            src: /nuxt-images/events/world-tour/speakers/Simon Mansfield.jpg

        - name: James Moverly
          title: Solutions Architect
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/James Moverly.jpg
          biography:
            text: |
              James is a Solutions Architect at GitLab. He has years of experience in the enterprise space, with a focus on mobile telecommunications. James is passionate about the DevOps mindset and helping others learn and grow. He is a strong advocate of sharing experiences and knowledge, whilst always looking for new ways to learn and improve on his quest.

        - name: Dominique Top
          title: Solutions Architect
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Dominique Top.png
          biography:
            text: |
              Dominique is a Solutions Architect at GitLab. She is a passionate individual with interests in music, technology, and giving back to the community.  Dominique prides herself on being a keen learner, solver of problems, and connector of dots and people.

              She is always interested in finding more efficient ways of doing things and helping others do the same. She is an Ambassador of OpenUK, an organisation advocating and promoting the use and development of Open Technologies in the UK. In addition, Dominique is a frequent attendee, as well as organiser of several DevOps related Meetups and Conferences, such as Docker London and DevOpsDays London.

              When she is not geeking out over the latest DevOps technologies, you can find her indulging in her love for cooking, playing video games, watching anime, or pursuing her passion for music by singing and spinning tracks as a techno DJ.
        - name: Stefania	Chaplin
          title: Solutions Architect
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Stefania Chaplin.jpg
          biography:
            text: |
              Stefania’s (aka DevStefOps) experience as a Solutions Architect within DevSecOps, Security Awareness and Software Supply Chain Management means she's helped countless organisations understand and implement security throughout their SDLC. As a python developer at heart, Stefania enjoys optimising and improving operational efficiency by scripting, automating and creating integrations. She is a member of OWASP DevSlop, hosting their technical shows and is regularly seen speaking at various events across the world. When not at a computer, Stefania enjoys surfing, yoga and looking after all her tropical plants.
      description:
        text: |
          If your team is looking to optimize your DevOps practices, join the DevOps Value Stream experts for valuable insights and actionable strategies. This interactive workshop will focus on discovery, identification and removal of blockers to drive visibility and continuous improvement. Dive deeper into Value Stream Assessments (VSA) and uncover areas of improvement in the SDLC. You will learn how you can accelerate value streams for faster business value realization. This interactive exercise will also provide practical experience for you to apply these VSA methodologies to your teams to identify key areas for improvement. At the end of this session, you will have a solid understanding of VSA principles, practical tools and techniques to set you ahead on the path to improving your DevOps processes.
    - time: 2:30 pm
      name: Break
    - time: 2:45 pm
      name: |
        Partner Spotlight: Eficode - How AI and DevOps change the way we create products in the future
      speakers:
        - name: Marko	Klemetti
          title: Chief Technology Officer
          company: Eficode
          image:
            src: /nuxt-images/events/world-tour/speakers/marko-klemetti.jpg
          biography:
            text: |
              Marko is the CTO of Eficode. He is also a founder and advisor in many tech startups. Marko is a passionate programmer who believes that design systems and continuous deployment are the enablers of a modern development organization.
      description:
          text: |
            As AI-assisted development becomes mainstream, organisations will have to find new ways to ensure their service quality remains high. Without the proper DevOps culture and practices in place, organisations will not be able to get the full benefits of this unseen velocity.

            This talk will explain how DevOps is building a safety net around modern software development, and how the right approach to Continuous Deployment and modern DevOps will help speed up development when using AI-powered tooling. This talk is a combination of a hands-on demo and explaining how to approach modern AI-powered DevOps.
    - time: 3:10 pm
      name: |
        **Roundtable discussion:** The human element of DevSecOps
      description:
          text: |
            ‘Technology isn’t the hard problem anymore, culture seems to be the chief problem now.’- Is this a statement that you have come across?

            We invite you to discuss with leaders the role and impact of people on DevOps processes. Dig deeper into why communication, collaboration and empathy play a critical role in effective DevOps strategies. What are the strategies that work while introducing DevOps into businesses. Does it work if you find the core problem the organization faces and start there? Is education on agile a great place to start when thinking about building a DevOps culture? Join us and weigh in on the best practices to foster teamwork, promote accountability and  develop a human-centered design for DevOps processes.

    - time: 3:40 pm
      name: Closing Remarks
      speakers:
        - name: Stephen Walters
          title: Field CTO
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Stephen Walters.png
    - time: 4:00 pm
      name: Snacks + Networking Reception
  sponsors:
    - img:  /nuxt-images/events/world-tour/speakers/eficode logo-black-transparent background.png
      alt: Eficode logo image
  form:
    header: Register for DevSecOps World Tour in London
    confirmation:
      - Thank you for registering!
      - Please check your email for a confirmation email.
    form_id: "3662"
  conduct_banner:
    header: Code of conduct
    text: GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](/company/culture/ecoc/) to ensure DevSecOps World Tour is a friendly, inclusive, and comfortable environment for all participants.
