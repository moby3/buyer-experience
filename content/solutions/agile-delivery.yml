---
  title: GitLab Agile Planning
  description: How to use GitLab as an agile project management tool for agile processes such as Scrum, Kanban, and Scaled Agile Framework (SAFe).
  report_cta:
    layout: "dark"
    title: Analyst reports
    reports:
    - description: "GitLab recognized as the only Leader in The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
    - description: "GitLab recognized as a Leader in the 2023 Gartner® Magic Quadrant™ for DevOps Platforms"
      url: /gartner-magic-quadrant/
  components:
    - name: 'solutions-hero'
      data:
        title: Agile Planning
        subtitle: Plan and manage your projects, programs, and products with integrated Agile support
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: Start your free trial
          url: /free-trial/
        secondary_btn:
          text: Learn about pricing
          url: /pricing/
          data_ga_name: Learn about pricing
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: gitlab for public sector"
    - name: 'copy-media'
      data:
        block:
          - header: Agile planning with GitLab
            text: |
              Development teams continue to accelerate value delivery with iterative, incremental, and lean project methodologies, such as Scrum, Kanban, and Extreme Programming (XP). Large enterprises have adopted Agile at enterprise scale through a variety of frameworks, including Scaled Agile Framework (SAFe), Spotify, and Large Scale Scrum (LeSS). GitLab enables teams to apply Agile practices and principles to organize and manage their work, whatever their chosen methodology.
            link_href: /demo/
            link_text: Learn more
            video:
              video_url: https://www.youtube.com/embed/kdE-yb6Puuo?si=qn2Sj_MTell0Btmi
          - header: GitLab benefits
            text: |
              As the most comprehensive DevSecOps platform, GitLab is:

              *   **Seamless:** GitLab supports collaboration and visibility for Agile teams — from planning to deployment and beyond — with a single user experience and a common set of tools
              *   **Integrated:** Manage projects in the same system where you perform your work
              *   **Scalable:** Organize multiple Agile teams to achieve enterprise Agile scalability
              *   **Flexible:** Customize out-of-the-box functionality to the needs of your methodology, whether you're rolling your own flavor of Agile or adopting a formal framework
              *   **Easy to learn:** See our [Quick Start](https://www.youtube.com/watch?v=VR2r1TJCDew){data-ga-name="quick start" data-ga-location="body"} guide and [tutorials](https://docs.gitlab.com/ee/tutorials/plan_and_track.html) on setting up Agile teams
            image:
              image_url: /nuxt-images/blogimages/cicd_pipeline_infograph.png
              alt: ""
          - header: Manage Agile projects
            inverted: true
            text: |
              GitLab enables lean and Agile project management from basic issue tracking to Scrum and Kanban-style project management. Whether you’re simply tracking a few issues or managing the complete DevSecOps lifecycle across a team of developers, GitLab has your team covered.

              *   **Plan, assign, and track** with [issues](https://docs.gitlab.com/ee/user/project/issues/index.html){data-ga-name="issues" data-ga-location="body"}
              *   **Organize work** with [labels](https://docs.gitlab.com/ee/user/project/labels.html){data-ga-name="labels" data-ga-location="body"}, [iterations](https://docs.gitlab.com/ee/user/group/iterations/){data-ga-name="iterations" data-ga-location="body"} and [milestones](https://docs.gitlab.com/ee/user/project/milestones/index.html#overview){data-ga-name="milestones" data-ga-location="body"}
              *   **Visualize work** with [boards](https://docs.gitlab.com/ee/user/project/issue_board.html){data-ga-name="boards" data-ga-location="body"} and [roadmaps](https://docs.gitlab.com/ee/user/group/roadmap/)
              *   **Correlate work with output** using [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/index.html){data-ga-name="merge requests" data-ga-location="body"}
            video:
              video_url: https://www.youtube.com/embed/-4SGhGpwZDY?si=5Wp8c_BN6AkB--kt
          - header: Manage programs and portfolios
            text: |
              Maintain visibility and control the people and projects aligned with business initiatives. Define and enforce policy and permissions, track progress and velocity across multiple projects and groups, and prioritize initiatives to deliver the greatest amount of value.

              *   **Organize** new business initiatives and efforts into [Epics](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="epics" data-ga-location="body"}
              *   **Align teams** and projects into programs—without sacrificing security and visibility—using Subgroups
              *   **Plan** [Sub-Epics](https://docs.gitlab.com/ee/user/group/epics/manage_epics.html#multi-level-child-epics){data-ga-name="sub-epics" data-ga-location="body"} and issues into Iterations and Milestones
              *   **Visualize** value delivery using Roadmaps, [Insights](https://docs.gitlab.com/ee/user/project/insights/){data-ga-name="insights" data-ga-location="body"}, and [Value Stream Analytics](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html){data-ga-name="value stream analytics" data-ga-location="body"}
            video:
              video_url: https://www.youtube.com/embed/8pLEucNUlWI?si=mnyDg4exa8MO-gGS
          - header: Scaled Agile Framework (SAFe) with GitLab
            inverted: true
            text: |
              See how your organization can use GitLab to build a framework using the Scaled Agile Framework (SAFe). Dive into the details around building out your Agile framework for development teams built on three pillars: team, program, and portfolio.
            video:
              video_url: https://www.youtube.com/embed/2IaMXteKSD4?si=tlfMuM6PC80io8zI
    - name: copy-media
      data:
        block:
          - header: Features
            miscellaneous: |
              *   **Issues:** Start with an [issue](https://docs.gitlab.com/ee/user/project/issues/){data-ga-name="start with an issue" data-ga-location="body"} that captures a single feature that delivers business value for users.
              *   **Tasks:** Often, a user story is decomposed into individual tasks. Create a [task](https://docs.gitlab.com/ee/user/tasks.html){data-ga-name="task" data-ga-location="body"} within an issue in GitLab, to further identify those individual units of work.
              *   **Epics:** Manage your portfolio of projects more efficiently and with less effort by tracking groups of issues that share a theme, across projects and milestones with [epics](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="milestones with epics" data-ga-location="body"}.
              *   **Iterations:** Create a cadence to assign issues to your team's sprints with [iterations](https://docs.gitlab.com/ee/user/group/iterations/){data-ga-name="iterations" data-ga-location="body"}. 
              *   **Issue boards:** Everything is in one place. [Track](https://docs.gitlab.com/ee/user/project/issues/){data-ga-name="track" data-ga-location="body"} issues and communicate progress without switching between products. One interface to follow your issues from backlog to done.
              *   **Roadmaps:** Start date and/or due date can be visualized in a form of a timeline. The epics [roadmap](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name="roadmap" data-ga-location="body"} page shows such a visualization for all the epics which are under a group and/or its subgroups.
              *   **Burndown Chart:** Track work in real time, and mitigate risks as they arise. [Burndown](https://docs.gitlab.com/ee/user/project/milestones/burndown_and_burnup_charts.html#burndown-charts){data-ga-name="burndown" data-ga-location="body"} charts allow teams to visualize the work scoped in a current sprint as they are being completed.
              *   **Collaboration:** The ability to [contribute](https://docs.gitlab.com/ee/user/discussions/){data-ga-name="contribute" data-ga-location="body"} conversationally is offered throughout GitLab in issues, epics, merge requests, commits, and more!
              *   **Traceability:** Align your team's issues with subsequent [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/){data-ga-name="merge requests" data-ga-location="body"} that give you complete traceability from issue creation to end once the related pipeline passes.
              *   **Wikis:** A system for documentation called [Wiki](https://docs.gitlab.com/ee/user/project/wiki/){data-ga-name="wiki" data-ga-location="body"}, if you are wanting to keep your documentation in the same project where your code resides.
              *   **Enterprise Agile Frameworks:** Large enterprises have adopted Agile at enterprise scale using a variety of frameworks. GitLab can support [SAFe](https://www.scaledagileframework.com/), Spotify, Disciplined Agile Delivery and more.
    - name: 'copy-media'
      data:
        block:
          - header: An Agile iteration with GitLab
            subtitle: User stories → GitLab issues
            text: |
              In Agile, you often start with a user story that captures a single feature that delivers business value for users. In GitLab, a single issue within a project serves this purpose.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue-list.png
              alt: ""
          - subtitle: Task → GitLab task
            inverted: true
            text: |
              Often, a user story is decomposed into individual tasks. You can create a task within an issue in GitLab, to further identify those individual units of work.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/Tasks.png
              alt: ""
          - subtitle: Epics → GitLab epics
            text: |
              Some Agile practitioners specify an abstraction above user stories, often called an epic, that indicates a larger user flow consisting of multiple features. In GitLab, an epic allows you to decompose work into multiple child sub-epics and issues to represent complex initiatives that span many groups and projects.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue.png
              alt: ""
          - subtitle: Product backlog → GitLab issue lists and prioritized labels
            inverted: true
            text: |
              The product or business owners typically create these user stories to reflect the needs of the business and customers. In GitLab, there are dynamically generated lists which users can view to track their backlog. Labels give flexibility to filter and tailor your view to reflect what is important for your team.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue-board.png
              alt: ""
          - subtitle: Sprints → GitLab iterations
            text: |
              A sprint represents a finite timebox in which the work is to be completed, which may be a week, a few weeks, or perhaps a month or more. GitLab iteration cadences can be used to capture your team's sprint cadences. Automated iteration management reduces the admnisitrative burden. 
            image:
              image_url: /nuxt-images/solutions/agile-delivery/Milestones.png
              alt: ""
          - subtitle: Points and estimation → GitLab issue weights
            inverted: true
            text: |
              In GitLab, issues have a weight attribute, which captures the estimated effort. Decompose an issue into tasks to perform more precise estimation. Time tracking allow teams to estimate and track time spent on issues. 
            image:
              image_url: /nuxt-images/solutions/agile-delivery/weight.png
              alt: ""
          - subtitle: Agile board → GitLab Issue Boards
            text: |
              Issue boards provide a central point for collaboration and to define your issue lifecycle. Issue boards help you to visualize and manage your entire process in GitLab. Use labels to create lists and filter data to create a view of your team's process.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue_board.png
              alt: ""
          - subtitle: Burndown charts → GitLab Burndown Charts
            inverted: true
            text: |
              With GitLab burndown charts teams can visualize the work scoped in a sprint "burning down" as it's being completed. At a glance, you see the current state for the completion a given iteration or milestone. 
            image:
              image_url: /nuxt-images/solutions/agile-delivery/burndown-chart.png
              alt: ""
    - name: copy-resources
      data:
        hide_horizontal_rule: true
        block:
          - subtitle: Are you ready for the next step?
            text: |
              Please see [get help for GitLab](/get-help/){data-ga-name="get help" data-ga-location="body"} if you have questions
            resources:
              blog:
                header: Blogs
                links:
                  - text: How to use GitLab for Agile
                    link: /blog/2018/03/05/gitlab-for-agile-software-development/
                    data_ga_name: GitLab for Agile
                    data_ga_location: body
                  - text: 4 ways to use Boards
                    link: /blog/2018/08/02/4-ways-to-use-gitlab-issue-boards/
                    data_ga_name: 4 ways to use Boards
                    data_ga_location: body
