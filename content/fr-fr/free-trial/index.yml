---
title: Testez GitLab gratuitement
description: Profitez de GitLab Ultimate avec l'essai gratuit de 30 jours et découvrez cet outil complet de développement logiciel et de DevOps doté d'une vaste gamme de fonctionnalités innovantes.
components:
  - name: call-to-action
    data:
      title: Essayez GitLab Ultimate gratuitement pendant 30 jours
      centered_by_default: true
      subtitle: L'essai gratuit inclut presque[[1]](#what-is-included-in-my-free-trial-what-is-excluded){class="cta__subtitle--subscript"} toutes les fonctionnalités de [l'édition Ultimate](/pricing/ultimate/){data-ga-name="Free trial includes nearly all Ultimate-tier" data-ga-location="header"}. Aucune carte de crédit n'est requise [[2]](/pricing/#why-do-i-need-to-enter-credit-debit-card-details-for-free-pipeline-minutes){class="cta__subtitle--subscript" data-ga-name="credit card faq" data-ga-location="header"}
      body_text: Commencez votre essai de 30 jours dès maintenant. Puis, profitez de GitLab Gratuit (pour toujours !).
      aos_animation: fade-down
      aos_duration: 500
  - name: free-trial-plans
    data:
      saas:
        tooltip_text: |
          Le logiciel en tant que service est un modèle de licence et de livraison de logiciels proposant le logiciel sous licence sur la base d'un abonnement et il est hébergé de manière centralisée.
        text: |
          GitLab est hébergé chez nous. Aucune configuration technique n'est requise. Démarrez tout de suite : aucune installation n'est nécessaire.
        link:
          text: Continuer avec GitLab SaaS
          href: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=free-trial
          data_ga_name: continue with saas
          data_ga_location: body
        bottom_text: |
          Vous avez déjà un compte ? [Connectez-vous](https://gitlab.com/users/sign_in?redirect_to_referer=yes&__cf_chl_jschl_tk__=db2d336ba94805d0675008cc3fa3e0882d90953c-1619131501-0-AeQCSleOFTDGa9C-lXa3ZZZPpsO6sh0lCBCPZT0GxdT7tyOMAZoPzKppSQq9eV2Gqq9_kwKB8Lt8GJQ-nF-ra8updJRDfWTMBAwCR-m38kaHdAJYTicvW8Tj4KH55GO25zOeCYJexeEp1hx6f3DMvtjZd8elp_RfdulgN4-rxW8-lFSumJdSzE8y8N9FGltpsoQ8SKFSq41jMoB_GJ1nkIrjCU_kaGxJA3l4xhh-C14XFoBoBtfGjGOH4Kj76Y5QAeT7qemwuGBlvpYCK0OBv5aPkFDZ_Knp0W1zaOkr5tt511fra-rE3ekQI_lwR5VqBTHLtNslfgt4Il1SKLi6ZJLkces_WsUWdIQ3jNlyKbv08CF6kyDI3NiEOcCXUopCfQDYr-5syEUhv1Cnxy-Vjn7u5ejR2pvwIytWm8io2rhcaSOYxzxWccpxZLfjotTkzlrNP7KALbkxQOcNa_zeWVQ5t6aGC8H5wrT8u8ICxuJC){data-ga-name="log in" data-ga-location="body"}
      self_managed:
        form_id: 2150
        form_header: GitLab est hébergé chez vous. Téléchargez et installez GitLab sur votre propre infrastructure ou dans un environnement cloud public. Une expérience de Linux est nécessaire.
  - name: faq
    data:
      aos_animation: fade-up
      aos_duration: 500
      header: FAQ concernant la version d'essai de GitLab
      texts:
        show: Afficher tout
        hide: Cacher tout
      groups:
        - header: Ce que comprend l'essai gratuit
          questions:
            - question: Qu'est-ce qui est inclus dans mon essai gratuit ? Qu'est-ce qui en est exclu ?
              id: what-is-included-in-my-free-trial-what-is-excluded
              answer: |
                Votre essai gratuit inclut presque toutes les fonctionnalités de notre [édition Ultimate](/pricing){data-ga-name="ultimate tier" data-ga-location="faq"}, à ces exceptions près :
                *Aucun service d'assistance n'est inclus dans l'essai gratuit. Si votre essai gratuit GitLab a pour but d'évaluer l'expertise du service d'assistance GitLab ou la performance de son SLA, veuillez [contacter notre service commercial](/sales/){data-ga-name="sales" data-ga-location="faq"} pour discuter des options possibles.
                *L'essai gratuit de GitLab SaaS est limité à 400 minutes de calcul par mois.
                *L'essai gratuit de GitLab SaaS en édition Premium et Ultimate incluent l'utilisation d'un seul [jeton de projet](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) disponible avec la licence d'essai.
                *L'essai gratuit de GitLab SaaS n'inclut pas l'utilisation de [jetons de groupe](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html).
            - question: Que se passe-t-il à la fin de ma période d'essai ?
              id: what-happens-after-my-free-trial-ends
              answer: |
                Votre période d'essai de GitLab Ultimate dure 30 jours. Une fois cette période écoulée, vous pouvez choisir de conserver un compte GitLab Gratuit pour toujours ou bien de passer à un [forfait payant](/pricing/){data-ga-name="paid plan" data-ga-location="faq"}.
        - header: Différences entre GitLab SaaS et GitLab Auto-géré
          questions:
            - question: Quelle est la différence entre les configurations de GitLab SaaS et de GitLab Auto-géré ?
              id: what-is-the-difference-between-saas-and-self-managed-setups
              answer: |
                GitLab SaaS est hébergé chez nous. Aucune configuration technique n'est requise, vous n'avez donc pas à vous soucier de télécharger et d'installer vous-même GitLab. GitLab Auto-géré est hébergé chez vous. Téléchargez et installez GitLab sur votre propre infrastructure ou dans un environnement cloud public. Une expérience de Linux est nécessaire.
            - question: Certaines fonctionnalités sont-elles incluses uniquement dans GitLab SaaS ou GitLab Auto-géré ?
              id: are-certain-features-included-only-in-saas-or-self-managed
              answer: |
                Certaines fonctionnalités ne sont disponibles qu'avec GitLab Auto-géré. Comparez la [liste complète des fonctionnalités ici](/pricing/feature-comparison/){data-ga-name="features list" data-ga-location="faq"}.
        - header: Tarifs et remises
          questions:
            - question: Une carte de crédit/débit est-elle nécessaire pour obtenir l'essai gratuit ?
              id: is-a-credit-debit-card-required-for-a-free-trial
              answer: |
                Une carte de crédit/débit n'est pas requise pour les clients qui n'utilisent pas GitLab.com CI/CD, qui n'installent pas leurs propres runners et qui ne désactivent pas les runners partagés. Cependant, vous devrez renseigner vos informations de carte bancaire si vous choisissez d’utiliser les runners partagés de GitLab.com. Nous avons apporté ce changement pour éviter l'utilisation abusive des minutes de calcul fournies gratuitement sur GitLab.com dans le but de miner des cryptomonnaies. En effet, ces abus ont créé des problèmes de performances pour les utilisateurs GitLab.com. Une transaction d'autorisation d'un montant de un dollar sera effectuée pour vérifier l'exactitude des informations bancaires fournies. Aucun frais ne vous sera facturé et aucune somme ne vous sera transférée. Pour en savoir plus, cliquez [ici](/blog/2021/05/17/prevent-crypto-mining-abuse/){data-ga-name="prevent crypto mining abuse" data-ga-location="faq"}.
            - question: Combien coûte une licence GitLab ?
              id: how-much-does-a-gitlab-license-cost
              answer: |
                Les informations relatives à l'abonnement sont disponibles sur notre [page de tarifs](/pricing/){data-ga-name="pricing page" data-ga-location="faq"}.
            - question: Proposez-vous des tarifs spéciaux pour les projets open source, les établissements d'enseignement ou les start-up ?
              id: do-you-have-special-pricing-for-open-source-projects--startups--or-educational-institutions
              answer: |
                Tout à fait ! Nous fournissons des licences Ultimate gratuites aux projets open source, établissements d'enseignement et start-up qui remplissent les conditions requises. Pour en savoir plus, consultez les pages de nos programmes [GitLab pour open source](/solutions/open-source/){data-ga-name="gitlab for open source" data-ga-location="faq"}, [GitLab pour les start-up](/solutions/startups/){data-ga-name="gitlab for startups" data-ga-location="faq"}, and [GitLab pour l'éducation](/solutions/education/){data-ga-name="gitlab for education" data-ga-location="faq"}.
            - question: Comment puis-je passer de GitLab Gratuit à l'un des abonnements payants ?
              id: how-do-i-upgrade-from-gitlab-free-to-one-of-the-paid-subscriptions
              answer: |
                Si vous souhaitez passer de GitLab Gratuit à l'une des éditions payantes, suivez les [guides inclus dans notre documentation](https://docs.gitlab.com/ee/update/#community-to-enterprise-edition){data-ga-name="community to enterprise" data-ga-location="faq"}.
        - header: Installation et migration
          questions:
            - question: Comment migrer vers GitLab à partir d'un autre outil Git ?
              id: how-do-i-migrate-to-gitlab-from-another-git-tool
              answer: |
                Consultez toutes les instructions de migration depuis les systèmes de contrôle de version les plus populaires dans [notre documentation](https://docs.gitlab.com/ee/user/project/import/index.html){data-ga-name="migration" data-ga-location="faq"}.
            - question: Comment installer GitLab en utilisant un conteneur ?
              id: how-do-i-install-gitlab-using-a-container
              answer: |
                Pour en savoir plus sur l'installation de GitLab à l'aide de Docker, consultez [notre documentation](https://docs.gitlab.com/omnibus/docker/README.html){data-ga-name="install docker" data-ga-location="faq"}.
        - header: Intégrations GitLab
          questions:
            - question: GitHost est-il toujours disponible ?
              id: is-githost-still-available
              answer: |
                Non, nous n'acceptons plus de nouveaux clients pour GitHost.
            - question: Avec quels outils GitLab s'intègre-t-il ?
              id: what-tools-does-gitlab-integrate-with
              answer: |
                GitLab propose un certain nombre d'intégrations tierces. Pour en savoir plus sur les services disponibles et sur la façon de les intégrer, consultez [notre documentation](https://docs.gitlab.com/ee/integration/README.html){data-ga-name="third party integration" data-ga-location="faq"}.
