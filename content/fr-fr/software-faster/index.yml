---
  title:  Des logiciels. Rapidement.
  description: GitLab simplifie votre DevSecOps afin que vous puissiez vous concentrer sur l'essentiel. En savoir plus
  title_image: /nuxt-images/open-graph/open-graph-gitlab.png
  twitter_image: /nuxt-images/open-graph/open-graph-gitlab.png
  hero:
    title: |
      Le chemin le plus rapide pour transformer une idée en logiciel
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 0
    image:
      url: /nuxt-images/software-faster/hero.png
      alt: image hero des logiciels-plus rapidement
      aos_animation: fade-up
      aos_duration: 1600
      aos_offset: 0
    secondary_button:
      video_url: https://player.vimeo.com/video/799236905?h=59b06b0e99&badge=0&autopause=0&player_id=0&app_id=58479
      text: Qu'est-ce que GitLab ?
      data_ga_name: watch video
      data_ga_location: hero
  customer_logos:
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: Lien vers la page d'accueil de T-Mobile et GitLab"
        alt: "Logo T-Mobile"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Lien vers l'étude de cas de Goldman Sachs
        alt: "Logo Goldman Sachs"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/home/logo_cncf_mono.svg"
        link_label: Lien vers l'étude de cas de la Cloud Native Computing Foundation
        alt: "Logo Cloud Native"
        url: /customers/cncf/
      - image_url: "/nuxt-images/home/logo_siemens_mono.svg"
        link_label: Lien vers l'étude de cas de Siemens
        alt: "Logo Siemens"
        url: /customers/siemens/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Lien vers l'étude de cas de Nvidia
        alt: "Logo Nvidia"
        url: /customers/nvidia/
      - alt: Logo UBS
        image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
        link_label: Lien vers l'étude de cas d'UBS
  featured_content:
    col_size: 4
    header: "Ensemble, c'est mieux\_: les clients livrent leurs logiciels plus rapidement avec GitLab"
    case_studies:
      - header: La transition rapide et transparente du Nasdaq vers le Cloud
        description: |
          Le Nasdaq a l'ambition de devenir 100 % cloud. Il s'est associé à GitLab pour y parvenir.
        showcase_img:
          url: /nuxt-images/software-faster/nasdaq-showcase.png
          alt: Logo Nasdaq sur une fenêtre
        logo_img:
          url: /nuxt-images/enterprise/logo-nasdaq.svg
          alt: Logo Nasdaq
        link:
          video_url: https://player.vimeo.com/video/767082285?h=e76c380db4
          text: Regarder la vidéo
          data_ga_name: nasdaq
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 800
      - header: Profitez d'un délai de déploiement 5 fois plus rapide
        description: |
          Hackerone a amélioré la durée de l'étape de pipeline, la vitesse de déploiement et l'efficacité de ses développeurs grâce à GitLab Ultimate.
        showcase_img:
          url: /nuxt-images/software-faster/hackerone-showcase.png
          alt: Personne qui travaille sur un ordinateur avec du code - HackerOne
        logo_img:
          url: /nuxt-images/logos/hackerone-logo.png
          alt: Logo HackerOne
        link:
          href: /customers/hackerone/
          text: En savoir plus
          data_ga_name: hackerone
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1000
      - header: Les fonctionnalités de la release sont 144 fois plus rapides
        description: |
          Airbus Intelligence a amélioré son flux de travail et la qualité de son code en utilisant une application CI tout-en-un.
        showcase_img:
          url: /nuxt-images/software-faster/airbus-showcase.png
          alt: Aile d'avion en vol - Airbus
        logo_img:
          url: /nuxt-images/software-faster/airbus-logo.png
          alt: Logo Airbus
        link:
          href: /customers/airbus/
          text: Lire cet article
          data_ga_name: airbus
          data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
  devsecops:
    header: Tous les outils DevSecOps essentiels inclus dans une plateforme complète
    link:
      text: En savoir plus
      data_ga_name: platform
      data_ga_location: body
    cards:
      - header: Des insights de meilleure qualité
        description: Visibilité de bout en bout sur l'ensemble du cycle de vie de la livraison logicielle.
        icon: case-study-alt
      - header: Une efficacité accrue
        description: Prise en charge intégrée de l'automatisation et des intégrations avec des services tiers.
        icon: principles
      - header: Collaboration améliorée
        description: Un seul flux de travail qui unit les équipes de développeurs, de sécurité et d'opérations.
        icon: roles
      - header: Un cycle plus court pour produire de la valeur
        description: Amélioration continue grâce à des circuits de commentaires accélérés.
        icon: verification
  by_industry_case_studies:
    title: Explorez les ressources DevSecOps
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: Édition 2023 de nos rapports mondiaux sur le DevSecOps
        subtitle: Découvrez ce que plus de 5 000 développeurs DevSecOps nous ont appris sur l'état actuel du développement logiciel et de la sécurité et des opérations associées.
        image:
          url: /nuxt-images/software-faster/devsecops-survey.png
          alt: icône enquête devsecops
        button:
          href: /developer-survey/
          text: Lire le rapport
          data_ga_name: devsecops survey
          data_ga_location: body
        icon:
          name: doc-pencil-alt
          alt: Icône d'un cercle de jeu
      - title: 'Tout réside dans la cadence : il faut des bureaux d''ingénierie 10x pour des ingénieurs 10x.'
        subtitle: Sid Sijbrandij, PDG et co-fondateur de GitLab, explique l'importance de la cadence au sein des bureaux d'ingénierie.
        image:
          url: /nuxt-images/blogimages/athlinks_running.jpg
          alt: photo de marathoniens qui courent
        button:
          href: /blog/2022/11/03/cadence-is-everything-10x-engineering-organizations-for-10x-engineers/
          text: Lire l'article de blog
          data_ga_name: read the blog
          data_ga_location: body
        icon:
          name: blog
          alt: Icône d'un cercle de jeu
