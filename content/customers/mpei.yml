---
  title: Moscow Power Engineering Institute
  description: Moscow Power Engineering Institute powers forward with GitLab-infused teaching @mpei_ru
  image_title: /nuxt-images/blogimages/mpei_cover_image.jpg
  image_alt: 
  twitter_image: /nuxt-images/blogimages/mpei_cover_image.jpg
  data:
    customer: Moscow Power Engineering Institute
    customer_logo: /nuxt-images/customers/mpei_logo.svg
    heading: Moscow Power Engineering Institute powers forward with GitLab-infused teaching
    key_benefits:
      - label: Enhanced collaboration
        icon: collaboration
      - label: Improved project management
        icon: enablement
    header_image: /nuxt-images/blogimages/mpei_cover_image.jpg
    customer_industry: Education
    customer_employee_count: 13,000 students
    customer_location: Moscow, Russia
    customer_solution: |
      [GitLab Self Managed Ultimate](/pricing){data-ga-name="pricing" data-ga-location="customers hero"}
    blurb: AI and math instructors at the Moscow Power Engineering Institute adopted GitLab to overcome workflow issues that hindered students’ moves to new DevOps methods.
    introduction: |
      Moscow Power Engineering Institute’s (MPEI) Department of Applied Mathematics counts on GitLab to organize student learning, with state-of-the-art DevOps processes.
    quotes:
      - text: |
          GitLab helps us to measure the performance of each student. It provides timely feedback
        author: Andrey Efanov
        author_role: Teaching Asst. and PhD student, Applied Mathematics and Artificial Intelligence Dept.
        author_company: MPEI
    content:
      - title: Preparing students for technical achievement
        description: |
          The National Research University “Moscow Power Engineering Institute” is a public university that offers training in power engineering, electrical and radio engineering, information technology, and related fields. The institute’s history is marked by profound technical achievement, such as being among the first to take radio images of the Venus surface and the dark side of the Moon. Within [MPEI](https://mpei.ru/lang/en/Pages/default.aspx){data-ga-name="MPEI" data-ga-location="body"} Department of Applied Mathematics, there has long been a special focus on research and teaching in the evolving fields of parallel programming and artificial intelligence — work that is evolving rapidly as cloud, Kubernetes, and microservices gain greater use.
      - title: MPEI looks to CI/CD pipelines to meld student efforts into cohesive whole
        description: |
          As they looked to grow researchers’ and students’ technical capabilities, MPEI educational leaders grew concerned. Software project complexity was increasing. New design paradigms such as containers and microservices were emerging. There were signs that assorted largely manual processes showed stress. Additionally, processes varied widely in methods applied. “Everybody used their own method,” said Andrey Efanov, teaching assistant and Ph.D. student in MPEI's Applied Mathematics and Artificial Intelligence Department. An essential requirement was support for capable CI/CD pipelines that could organize individual members into cohesive groups, Efanov added. It was beneficial generally to invite a large group into curated projects to participate. At times, however, there was a need to manage students’ visibility into each other’s projects. In this regard, tools such as Travis CI, GitHub, Moodle, and Google Classroom were found to be suboptimal. Able role-based access was a must.
      - title: Teachers and students alike gain insight into workflow
        description: |
          To expand innovation and enhance learning, the MPEI educators decided they needed an open-source DevOps platform they could flexibly apply to coursework and project work alike. GitLab met the MPEI Math and AI Department’s objectives in terms of highly granular role-based access control, integration, collaboration, and DevOps agility. By providing advanced planning capabilities in the form of milestones, epics, and boards, student project management is significantly enhanced. A GitLab group graph displays a useful visual history of repository work, which assures teachers and students a good understanding of general workflow. These group graphs include data on pushed events, opened and closed issues, and total number of contributions. From students’ perspectives, they gained greater insight into the development process. GitLab helped the department meet its requirement to automate the verification processes of student work, according to Efanov.
      - title: GitLab supports learning while doing, aids Kubernetes learning curve
        description: |
          Today, GitLab provides exceptional project visibility, and helps ongoing management of student work. It allows increased collaboration between students, and prepares them for industry careers deploying state-of-the-art CI/CD pipelines. GitLab group and subgroup hierarchies have become a part of the department’s organizational structure. There are now four learning and infrastructural services fully managed by just two staff engineers using GitLab IaC integration with Kubernetes: keycloak, wiki, a discord bot and a self-made online judge.
          
          Moreover, the GitLab environment simplifies Kubernetes development for beginners, who can make simple Helm child charts from templates to create services ready to deploy, Efanov said. “GitLab helps us to measure the performance of each student and it provides timely feedback. Sometimes, at the end of the term, a student would come up with some huge problems with which they had struggled for a long time,” he continued. “GitLab’s visibility means we can always ask what problems they face, if they are okay with them, or whether they need help with them.” While the institute provides important theoretical knowledge to students, the GitLab environment helps them expand that with practical experience in methods used by industry experts for creating better software.
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
        ga_name: all customers
        ga_location: customers stories
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
          ga_location: customers stories
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens/
          ga_location: customers stories
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
          ga_location: customers stories
