### Experiment Description
<!-- A detailed description of the experiment. --> <br>

Link to Test Design: <!--[link]-->

**Steps:** 
- [ ] Monitor the experiment until the p-value is below 0.05 
- [ ] Review on-page and down-funnel conversion metrics 
- [ ] Inform the Product Manager with the declared winner
- [ ] The Product Manager informs the Engineer to set the winning version live 
- [ ] If needed: Review ARR post 40-day maturity (date: mm/dd/yy) 
- [ ] Finalize results 

**Business KPIs:**
- KPI 1
- KPI 2

**Secondary Metrics:**
- Metric 1
- Metric 2

/confidential
/label  ~"dex::ab-testing" 
