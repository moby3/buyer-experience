---
  title: GitLab Cookie ポリシー
  description: このページでは、GitLab Cookie ポリシーに関する情報を見つけることができます。もっと詳しく知る！
  side_menu:
    anchors:
      text: "このページでは"
      data:
        - text: なぜクッキーを使用するのですか？
          href: "#why-do-we-use-cookies"
          data_ga_name: why do we use cookies
          data_ga_location: side-navigation
        - text: GitLabでクッキーを設定するのは誰ですか？
          href: "#who-sets-cookies-on-gitlab"
          data_ga_name: who sets cookies on gitlab
          data_ga_location: side-navigation
        - text: クッキーを管理するにはどうすればよいですか？
          href: "#how-do-i-manage-my-cookies"
          data_ga_name: how do i manage my cookies
          data_ga_location: side-navigation
        - text: 信号を追跡しない
          href: "#do-not-track-signals"
          data_ga_name: do not track signals
          data_ga_location: side-navigation
        - text: グローバルプライバシーコントロール
          href: "#global-privacy-control"
          data_ga_name: global privacy control
          data_ga_location: side-navigation
        - text: 自主規制プログラム
          href: "#self-regulatory-programs"
          data_ga_name: self regulatory programs
          data_ga_location: side-navigation
        - text: ウェブビーコン
          href: "#web-beacons"
          data_ga_name: web beacons
          data_ga_location: side-navigation
        - text: モバイル広告ID
          href: "#mobile-advertising-ids"
          data_ga_name: mobile advertising ids
          data_ga_location: side-navigation
        - text: どのような種類のクッキーが使用されますか？
          href: "#what-types-of-cookies-do-we-use"
          data_ga_name: what types of cookies do we use
          data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []
  hero_title: GitLab Cookie ポリシー
  disclaimer:
    text: |
      業界アナリストレポートの引用
    details: |
      英語のテキストとこの翻訳の間に矛盾がある場合は、英語版が優先されるものとします。
  copy: |
      本クッキーポリシーは、当社のプライバシーに関する声明に含まれる情報を補足し、GitLabのウェブサイト、製品、サービス（総称して「サービス」）を管理および提供するために当社がクッキーや関連テクノロジーをどのように使用するかを説明しています。

      クッキーは、お使いのコンピュータ上に保存される小さなテキストファイルです。ピクセルは、ウェブページ上のグラフィックイメージなどのコンテンツを配信する方法を提供する、ウェブページまたは電子メール上の少量のコードです。
  sections:
    - header: なぜクッキーを使用するのですか？
      id: why-do-we-use-cookies
      text: |
        GitLabは次の目的でクッキーを使用しています：

        - サービスへのアクセスと使用を補助する；
        - 訪問者が当社のサービスをどのように利用し、関与しているかを理解する；
        - 環境設定を実行する；
        - インタレストベース広告を配信する；
        - メールを開いて実行した操作を理解する；および
        - 当社のサービスを分析し、改善する。
    - header: GitLabでクッキーを設定するのは誰ですか？
      id: who-sets-cookies-on-gitlab
      text: |
        クッキーは、GitLabがファーストパーティクッキーを配置する場合と、インタレストベース広告を提供するGoogle Analyticsなどのサービスプロバイダーが設定する場合があります。サービスプロバイダーがサードパーティクッキーを設定すると、そのクッキーはサービスプロバイダー自身の目的を達成するだけでなく、GitLabへサービスや機能を提供します。GitLabは、サードパーティクッキーがどのように使用されるかを制御できません。Google Analytics Browser Add-OnのようなGoogle Analyticsで利用可能なオプトアウトを確認するには、<https://tools.google.com/dlpage/gaoptout/>を確認してください。
    - header: クッキーを管理するにはどうすればよいですか？
      id: how-do-i-manage-my-cookies
      text: |
        GitLabは、すべてのGitLabドメインにわたってクッキー管理ツールを統合し、クッキーの設定を一元管理できる場所をユーザーに提供しています。サービスの動作に不可欠な「絶対に必要な」クッキーを除き、他のすべてのクッキーカテゴリについてオプトイン、オプトアウト、またはクッキー設定を調整することができます。クッキーによっては無効にすると、サービス機能の一部が正しく動作しなくなる場合があることに注意してください。このツールは、「会社」の下の各ウェブページのフッターにある[**クッキー設定**]リンク（「カリフォルニア州居住者向けに**個人情報を販売または共有しない**」というタイトル ）をクリックすると確認することができます。

        ほとんどのウェブブラウザでは、すでに配置されているクッキーを削除できます。ただし、削除することで広告設定を含むクッキーが制御している設定や環境設定が削除されることがあります。お使いのブラウザのクッキーフォルダに作成されたクッキーを削除する方法については、<https://allaboutcookies.org/manage-cookies/>をご確認ください
    - header: 信号を追跡しない
      id: do-not-track-signals
      text: |
        「Do Not Track（追跡しない）」（DNT）は、追跡されたくないことを示すためにウェブブラウザで設定できるプライバシー設定です。GitLabはDNT信号に応答し、クライアント側イベントに対するユーザーのトラック設定を尊重します。
    - header: グローバルプライバシーコントロール
      id: global-privacy-control
      text: |
        DNT信号と同じく、「グローバルプライバシーコントロール」(GPC)はウェブブラウザーで設定できるプライバシー設定で、ユーザーの同意なしに個人情報を独立したサードパーティと共有または販売を拒否することをウェブサイトに通知します。GitLabは、該当する法律で対応が定められている管轄区域においてGPCを尊重します。
    - header: 自主規制プログラム
      id: self-regulatory-programs
      text: |
        サービスプロバイダーは、インタレストベース広告をオプトアウトする方法を提供する自主規制プログラムに参加している場合があり、ここからアクセスできます：

        - アメリカ合衆国：NAI（<http://optout.networkadvertising.org>）および（<http://optout.aboutads.info/>）
        - カナダ：Digital Advertising Alliance of Canada（<https://youradchoices.ca/>）
        - ヨーロッパ：European Digital Advertising Alliance（<http://www.youronlinechoices.com> ）
    - header: ウェブビーコン
      id: web-beacons
      text: |
        ほとんどのメールクライアントには画像の自動ダウンロードを防止する設定があり、開いたメールメッセージ内のウェブビーコンを無効にします。
    - header: モバイル広告ID
      id: mobile-advertising-ids
      text: |
        モバイルデバイスでは、プラットフォームによって提供される広告IDが収集され、クッキーIDと同様に使用されることがあります。iOSおよびAndroidオペレーティングシステムでは、追跡を制限したり、広告IDをリセットしたりできる制御機能を使用できます。
    - header: どのような種類のクッキーが使用されますか？
      id: what-types-of-cookies-do-we-use
      text: |
        GitLabは、厳密に必要なクッキー、機能性クッキー、パフォーマンスクッキー、分析クッキー、ターゲティングクッキー、広告クッキーなどの4つのカテゴリのクッキーを使用しています。これらの各クッキーの詳細を確認したり、サービス内にあるカテゴリーごとのクッキーを確認するには、各ウェブページで「会社」の下のフッターにある[**クッキー設定]リンク**（カリフォルニア居住者向けの**「個人情報を販売または共有しない」というタイトル**）をクリックしてください。