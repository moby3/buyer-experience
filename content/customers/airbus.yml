---
  title: Airbus
  description: Learn how Airbus Intelligence improved their workflow and code quality with GitLab @Airbus
  image_title: /nuxt-images/blogimages/airbus_cover_image.jpg
  image_alt: Airbus takes flight with GitLab releasing features 144x faster
  twitter_image: /nuxt-images/blogimages/airbus_cover_image.jpg
  data:
    customer: Airbus
    customer_logo: /nuxt-images/software-faster/airbus-logo.png
    heading: Airbus takes flight with GitLab releasing features 144x faster
    key_benefits:
      - label: One unified workflow
        icon: gitlab-release
      - label: Improved code quality
        icon: devsecops
      - label: Faster releases
        icon: speed-alt
    header_image: /nuxt-images/blogimages/airbus_cover_image.jpg
    customer_industry: Aerospace
    customer_employee_count: 130,000
    customer_location: Worldwide
    customer_solution: |
      [GitLab Self-Managed Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: merge requests per year on average
        stat: 10,000
      - label: project growth in 5 years
        stat: 425%
      - label: faster feature releases
        stat: 144x
    blurb: Airbus Intelligence is a global leader in the geospatial industry that needed a platform for effective collaboration.
    introduction: |
      With GitLab's single-application continuous integration (CI), Airbus Intelligence has improved their workflow and code quality.
    quotes:
      - text: |
          It’s simple. All teams operate around this one tool. Instantly, that made communication easier. We wouldn’t be where we are today if we didn’t have GitLab in our stack
        author: Logan Weber
        author_role: Software Automation Engineer
        author_company: Airbus Defense and Space, Intelligence
    content:
      - title: A global pioneer in aerospace
        description: |
          [Airbus Intelligence](https://www.intelligence-airbusds.com/) is a leading provider of commercial satellite imagery and premium geospatial data services as well as innovative defence solutions. The company's products and services support decision makers worldwide to increase security, optimise mission planning, boost performance, improve management of natural resources, and protect the environment.
      - title: Adopt a better developer workflow
        description: |
          As a multinational corporation, Airbus Intelligence needs tools that can help their team collaborate and work more efficiently across the globe. The Intelligence business wanted to avoid the common challenges of many global companies: distributed teams and disconnected toolchains that cause workflow inefficiency and slow production. An improved workflow that could break through these challenges, make teams more efficient, and foster communication was a high priority. Logan Weber is a software automation engineer at the Web Factory. Finding a better developer workflow was one of his core missions, and the Web Factory’s agility makes the team an ideal testing ground for new tools and technologies. For Logan, it was important that any tools they adopt share a similar dedication to innovation. “We’re in the midst of a digital transformation,” Logan said. “We want to join forces with partners who know what they’re doing and can keep up with us.”

          One of the Web Factory team’s big challenges was that their processes just weren’t efficient enough, which led to delayed releases and lost time in development. Developers could spend at least a full day on the production setup, and too much time was being spent on simple tasks that should have been automated. Developers were frustrated with these manual and lengthy processes that stopped them from focusing on code. With a new tool the Web Factory team also hoped to avoid communication breakdowns between teams. After spending time on the production setup, developers would sometimes realise that the final product didn’t correspond to the initial request, which would then lead to additional efforts. “We’d have to create a bug to modify this error. But it wasn’t a bug, it was just a lack of communication,” Logan explained.

          The Web Factory team tested several tools in the search for the right developer workflow. Because the Web Factory team already used Jira, they decided to test other Atlassian products, such as Bitbucket for version control and Bamboo for CI. Unfortunately, BitBucket and Bamboo didn’t offer a user-friendly experience, and both tools lacked some of the functionalities for their needs. The Web Factory team used Jenkins on old projects, but found it too complicated to maintain. They also wanted to be able to store their deployment script processes as code.
      - title: A unified GitLab workflow
        description: |
          “There was a bit of anything and everything, but we just couldn’t find what we were looking for,” Logan said. After trying other tools, the Web Factory team chose GitLab because it offers several advantages over the other tools tested. Not only does GitLab offer version control and project management capabilities, it also provides best-in-class CI — all in a single application. The Web Factory uses a Scrum methodology with two-week sprints. Developers create a user story in Jira, and once they’re ready to work on it, they create an issue in GitLab. Once teams have finished gathering information and collaborating in issues, they create a merge request (MR) which will trigger a development branch. When developers are ready, they can ask other developers to review their code. The code will go through CI testing and, once all tests are passed, the reviewer can merge this development branch into the main branch. Because all of this goes through the MR, everyone can see the entire process from start to finish. And GitLab offered the team a way to store their deployment scripts as code using the `.gitlab-ci.yml` file — one of the team’s must-have features.

          For developers, having security and vulnerability scans built into the integration testing was also very helpful. “What used to happen is we would touch one part of the code and it would break another part. Now, each time a developer pushes code, we can immediately identify problems,” Logan said. With issues and MRs, people across teams have a place to collaborate. With CI included, teams can view every project from start to finish, and this visibility has also removed the guesswork from deployments. Instead of relying on one person to manage a deployment because they were more involved or more knowledgeable about the specific project, anyone can deploy because they have the same visibility as everyone else.
      - title: Better code quality, improved collaboration, happy developers
        description: |
          The first success the Web Factory noticed after adopting GitLab was the improvement in code quality. GitLab CI’s built-in security testing meant that developers could now identify bugs and vulnerabilities before they reached production. With GitLab CI, the Web Factory team was also able to deploy more frequently and with confidence. Instead of spending a full day setting up for production and doing manual tests, those simple tasks are now automated. This allowed release time to go from 24 hours to just 10 minutes. Today, Logan estimates that 98% of releases happen on time and the remaining 2% happen only a few hours later — a vast improvement from before.

          Collaboration improved because everyone can communicate in one place. Now, any technical stakeholder knows what’s being worked on, where everything is in the process, and developers know where to find the information they need to do their jobs. Developers, designers, security, and operations teams all have a place in the tool. While the improvements in code quality, cycle times, and communication were expected, there were also some unexpected benefits to adopting GitLab that were pleasant surprises for Logan and the Web Factory team: happier developers.

          For one, GitLab CI took the stress out of deployments. “When someone went on vacation, it could be hell,” Logan laughed. And with better automation, developers can now focus on upgrades, technically demanding tasks, and updates. What else can developers do with this newfound free time? “We can create features!” Logan said. “We create and improve features for applications, and there just wasn’t any time for that before. We have 17 applications, and now developers can stay focused on the important things.”

          Happier developers also had a positive impact on recruitment. “[The Web Factory] had a hard time recruiting developers previously, but now that GitLab is a part of the tech stack, we’re receiving more applications from more experienced developers.” Logan explained. When developers can focus on their jobs, attracting other talented developers becomes easier. For Logan, adopting GitLab changed the entire development process for the better. The Web Factory team has been using GitLab for three years now, and Logan is a GitLab Hero who regularly shares his experience and expertise with others. If his team hadn’t adopted GitLab, he doesn’t think they would be as efficient. “We’d still have long processes, long periods of development, and less and less motivated developers who no longer like the projects they are working on and end up leaving,” Logan summarizes.
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
        ga_name: all customers
        ga_location: customers stories
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
          ga_name: goldman sachs
          ga_location: customers stories
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens/
          ga_name: siemens
          ga_location: customers stories
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
          ga_name: fanatics
          ga_location: customers stories
