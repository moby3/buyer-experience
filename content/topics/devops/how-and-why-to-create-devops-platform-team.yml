---
  title: What a DevOps platform team can do for your organization
  description: "If your DIY DevOps effort is overwhelmed by infrastructure support needs, it’s time to consider a cutting-edge addition: a DevOps platform team."
  partenttopic: devops
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  date_published: 2023-03-23
  date_modified: 2023-03-30
  topics_header:
      data:
        title:  What a DevOps platform team can do for your organization
        block:
          - metadata:
              id_tag: what-is-devops
            text: |
              If your DIY DevOps effort is overwhelmed by infrastructure support needs, it’s time to consider a cutting-edge addition: a DevOps platform team.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: DevOps
      href: /topics/devops/
      data_ga_name: devops
      data_ga_location: breadcrumb
    - title: How and why to create a DevOps platform team
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: Start with the DevOps platform
          href: "#start-with-the-dev-ops-platform"
          data_ga_name: Start with the DevOps platform
          data_ga_location: side-navigation
          variant: primary
        - text: What a DevOps platform team can do
          href: "#what-a-dev-ops-platform-team-can-do"
          data_ga_name: What a DevOps platform team can do
          data_ga_location: side-navigation
        - text: It’s not platform engineer vs. DevOps
          href: "#it-s-not-platform-engineer-vs-dev-ops"
          data_ga_name: It’s not platform engineer vs. DevOps
          data_ga_location: side-navigation
        - text: What’s different about platform teams?
          href: "#what-s-different-about-platform-teams"
          data_ga_name: What’s different about platform teams?
          data_ga_location: side-navigation
    content:
      - name: topics-copy-block
        data:
          header: DevOps roles are evolving
          blocks:
            - column_size: 8
              text: |
                Adopting a DevOps platform won’t just improve cycle times – it also provides an opportunity to rethink traditional roles, particularly on the ops side. [Our 2022 Global DevSecOps Survey](/developer-survey/) shows that all DevOps roles are changing, but that was especially true in operations. Ops pros told us they were taking on new responsibilities including managing the cloud, maintaining the toolchain, DevOps coaching, automation, and platform engineering. Some organizations are going further and creating a DevOps platform team to help with the unique challenges of advanced DevOps.

                Here’s a look at why, and how one could fit into a [DevOps](/topics/devops/) organization.
      - name: topics-copy-block
        data:
          header: Start with the DevOps platform
          blocks:
            - column_size: 8
              text: |
                Less is certainly more when it comes to a [DevOps platform](/solutions/devops-platform/); it brings all the steps necessary to develop, secure, and release software together in one place and ends the toolchain “tax.” The platform can serve up advanced technologies from Kubernetes to microservices and infrastructure as code (IaC), and as such, it needs an owner. In the past, a site reliability engineer (SRE) might have been tasked with some of those responsibilities, but today some organizations are looking to hire DevOps platform engineers in order to create a DevOps platform team.

                Not every company with a DevOps platform will need a team, however. An organization without legacy systems might not need this level of focus on infrastructure, while one with both cloud environments and on-premises data centers will likely want extra help supporting all the moving parts.
      - name: topics-copy-block
        data:
          header: What a DevOps platform team can do
          blocks:
            - column_size: 8
              text: |
                At its heart, a DevOps platform team will free their internal customers — ops (and devs for that matter) — from the complex and sometimes messy struggle to support the infrastructure. The goal is, of course, to offer as much self-service as possible for dev and ops, which means a streamlined process and less touchy experience. A DevOps platform team can “tame the beast,” making it possible for devs to do push-button deployments without any extra involvement.

                A DevOps platform team will likely take full advantage of infrastructure as code so manual interventions aren’t required. Devs will benefit from an API-interface that will allow them to do their jobs without actually having to understand how the infrastructure is created.

                For some organizations, a DevOps platform team is a way to [maximize engineering efficiency](https://hackernoon.com/how-to-build-a-platform-team-now-the-secrets-to-successul-engineering-8a9b6a4d2c8), and for others it allows for a focus on best practices, an end to ad-hoc platform “volunteer managers” who won’t have a broad view of business goals, and an increase in business agility.
      - name: topics-copy-block
        data:
          header: It’s not platform engineer vs. DevOps
          blocks:
            - column_size: 8
              text: |

                A platform engineering team is an extension of a DevOps team, not a replacement for it. Some practitioners warn of the risks of accidentally creating a secondary DevOps team while trying to create a platform team.

                Also, it’s important to keep in mind that [platform engineers](/topics/devops/what-is-a-devops-platform-engineer/) need a broad set of skills ranging from security to Linux to Kubernetes, not to mention soft skills like communication and collaboration. They should be laser focused on infrastructure, not product development.
      - name: topics-copy-block
        data:
          header: What’s different about platform teams?
          blocks:
            - column_size: 8
              text: |
                A platform engineering team is a central team focused on building, maintaining, and optimizing the core systems — the DevOps platform or other software development tools and cloud infrastructure, in particular — that enable other teams to ship high quality software securely and regularly to the organization’s external customers. The platform team’s customers are internal and often include development, operations, product management, and product teams.

                Here are some of the things they may be working on at any given time:
                * Finding new solutions to modernize or replace old systems and legacy tools, and helping other teams migrate to them
                * Making different cloud service providers work together better
                * Building a platform that helps the organization more fully adopt practices like continuous integration and continuous delivery (CI/CD) and automated testing
                * Working on ways to continuously improve the stability, efficiency and performance of the platform
  components:
    - name: solutions-resource-cards
      data:
        title: More on DevSecOps platforms
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: Blog
            header: Future-proof your developer career
            image: /nuxt-images/blogimages/axway-case-study-image.png
            link_text: "Learn more"
            href: /blog/2020/10/30/future-proof-your-developer-career/
            data_ga_name:
            data_ga_location: body
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: Blog
            header: "Auto DevOps 101: How we're making CI/CD easier"
            image: /nuxt-images/blogimages/autodevops.jpg
            link_text: "Learn more"
            href: /blog/2019/10/07/auto-devops-explained/
            data_ga_name:
            data_ga_location: body
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: Blog
            header: A beginner's guide to continuous integration
            image: /nuxt-images/blogimages/scm-ci-cr.png
            link_text: "Learn more"
            href: /blog/2018/01/22/a-beginners-guide-to-continuous-integration/
            data_ga_name:
            data_ga_location: body
