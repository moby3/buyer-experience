---
  title: GitLab CI/CD for GitHub
  description: GitHubの統合により、GitLabユーザーは外部のGitHub.comまたはGitHub Enterpriseコードリポジトリに接続されたGitLabでCI/CDプロジェクトを作成できるようになりました。
  components:
    - name: 'solutions-hero'
      data:
        title: GitHub用GitLab CI/CD
        subtitle: GitHubでコードをホストします。 GitLabでのビルド、テスト、デプロイ。
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html
          text: 文書
          data_ga_name: github integration
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "イメージ: GitHub用GitLab CI/CD"
    - name: copy
      data:
        block:
          - header: ビルドとテストを自動化
            id: automate-build-and-test
            text: |
                GitHubの統合により、GitLabユーザーは外部のGitHub.comまたはGitHub Enterpriseコードリポジトリに接続されたGitLabでCI/CDプロジェクトを作成できるようになりました。これにより、コードがGitHubにプッシュされるたびにGitLab CI/CDが自動的に実行され、完了するとCI/CDの結果がGitLabとGitHubの両方に返されます。
    - name: copy-media
      data:
        block:
          - header: 誰のためのGitLab CI/CD for GitHubですか？
            id: who-is-git-lab-ci-cd-for-git-hub-for
            text: |
              ### Open source projects
              GitHubに公開されたオープンソースプロジェクトがある場合は、GitLab.comで無料のCI/CDを利用できるようになりました。 オープンソースへの取り組みの一環として、すべての公開プロジェクトに最高レベルの機能(GitLab SaaS Ultimate)を無料で提供しています。 他のCI/CDベンダーが、同時実行できるジョブをほんの一握りに制限しているのに対し、[GitLab.com](https://gitlab.com){data-ga-name="gitlab.com" data-ga-location="open source projects CI"}は、オープンソースプロジェクトに数百の同時実行ジョブと50,000の無料計算分を提供しています。

              ### Large Enterprises
              最大手の顧客に話を聞くと、多くの場合、さまざまなチームがさまざまなツールを使っているそうです。GitLabでCI/CDを標準化したいと考えていますが、コードはGitLab、GitHub、その他のリポジトリに保存されています。 この機能により、企業はすべての異なるリポジトリで共通のCI/CDパイプラインを使用できるようになりました。 これは主要な利用者であり、私たちがGitHubのCI/CDを自己管理プレミアムプランの一部にした理由でもあります。

              ### Anyone using GitHub&#46;com
              GitLabはSCMとCI/CDを同じアプリケーションで使えるようにデザインされていますが、GitHubのバージョン管理でGitLabのCI/CDを使うことの魅力も理解しています。したがって、来年はGitLab CI/CD for GitHub機能を[GitLab.com](https://gitlab.com){data-ga-name="gitlab.com" data-ga-location="anyone using github.com"}無料プランの一部にします。つまり、個人的なプロジェクトやスタートアップから中小企業まで、誰でもGitHubを無料で使うことができます。 400分間無料で開始できるように、ユーザーは[自分のRunnerを追加](https://docs.gitlab.com/ee/ci/runners/README.html#registering-a-specific-runner){data-ga-name="add runners" data-ga-location="body"}したり、プランをアップグレードしてさらに多くの機能を利用できます。

              ### Gemnasium customers
              最近[Gemnasiumを買収](/press/releases/2018-01-30-gemnasium-acquisition.html){data-ga-name="gemnasium" data-ga-location="body"}しました。このような素晴らしいチームが私たちのチームに加わることに非常に嬉しく思います。そしてGemnasiumを使用していた人々をサポートし、移行パスを提供したいと考えています。 組み込みのセキュリティスキャンの一環として、すでに[Gemnasiumの機能](/releases/2018/02/22/gitlab-10-5-released/#gemnasium-dependency-checks){data-ga-name="gemnasium features" data-ga-location="body"}が出荷されています。現在、GitLab CI/CD for GitHubを使用すると、GitHub + Gemnasiumを使用していたGemnasiumの顧客は、コードを移行することなく、セキュリティ上のニーズに応じてGitLab CI/CDを使い始めることができます。
            image:
              image_url: /nuxt-images/logos/github-logo.svg
              alt: ""
          - header: メリット
            id: benefits
            inverted: true
            text: |
              GitLab CI/CD for GitHubを使用すると、ユーザーは外部のGitHubコードリポジトリに接続されたGitLabでCI/CDプロジェクトを作成できます。これにより、複数のコンポーネントが自動的に構成されます。

              * リポジトリの[プルミラーリング](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pulling-from-a-remote-repository){data-ga-name="pull mirroring" data-ga-location="body"}。
              * GitLabへのプッシュWebhookは、コードがコミットされるとすぐにCI/CDをトリガーします。
              * GitHubプロジェクトサービスインテグレーションのWebhooks CIステータスをGitHubに戻します。
            image:
              image_url: /nuxt-images/features/github-status-github.png
              alt: ""
          - header: 外部リポジトリ用GitLab CI/CD
            text: |
              GitLabはGitHubと統合するだけでなく、プロジェクトにURLによるリポジトリを追加してWebhookを構成することで、任意のベンダーの任意の外部gitリポジトリからCI/CDを実行することもできます。たとえば、[GitLab CI/CDを使用するようにBitbucketを構成](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/bitbucket_integration.html){data-ga-name="bitbucket for gitlab" data-ga-location="body"}できます。

              [外部リポジトリ用GitLab CI/CD](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/){data-ga-name="ci/cd for external repositories" data-ga-location="body"}のドキュメントをお読みください。
            image:
              image_url: /nuxt-images/logos/git-logo.svg
              alt: ""
          - header: プランと価格
            id: plans-and-pricing
            inverted: true
            text: |
              GitLab CI/CD for GitHubは別途価格ではなく、GitLabの標準的なエンドツーエンド製品の機能としてバンドルされています。

              **自己管理インストール**の場合、GitLab CI/CD for GitHubは、**Premium**と**Ultimate**ライセンスプランをお持ちのお客様にご利用いただけます。

              GitLab CI/CD for GitHubは、2020年3月22日まで**無料**ティアでプロモーションとして利用できます。(2020年3月22日以降、この機能は**Premium**ランクに移行し、**Premium**と**Ultimate**のユーザーが利用できるようになります。)

              GitLabサブスクリプションオプションの詳細については、[価格ページ](/pricing/){data-ga-name="pricing" data-ga-location="body"}をご覧ください。
            icon:
              name: checklist
              alt: チェックリスト
              variant: marketing
              hex_color: '#F43012'
          - header: 詳しく見る
            id: learn-more
            text: |
              * GitLab CI/CD: [GitLab CI/CDの利点](/solutions/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"}の詳細をご覧ください。
              * ドキュメント: [文書化](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html){data-ga-name="documentation" data-ga-location="body"}を始めましょう。
              * リリース: [GitLab 10.6](/releases/2018/03/22/gitlab-10-6-released/){data-ga-name="gitlab 10.6" data-ga-location="body"}のリリースポストをお読みください。
            image:
              image_url: /nuxt-images/logos/gitlab-logo.svg
              alt: ""
