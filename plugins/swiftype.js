// Adapted from the Swiftype quick start documentation: https://swiftype.com/documentation/site-search/crawler-quick-start
/* eslint-disable func-names, no-param-reassign, no-undef */
(function (w, d, t, u, n, s, e) {
  w.SwiftypeObject = n;
  w[n] =
    w[n] ||
    function () {
      // eslint-disable-next-line prefer-rest-params
      (w[n].q = w[n].q || []).push(arguments);
    };
  s = d.createElement(t);
  e = d.getElementsByTagName(t)[0]; // eslint-disable-line prefer-destructuring
  s.async = 1;
  s.src = u; // eslint-disable-line prefer-destructuring
  e.parentNode.insertBefore(s, e);
})(window, document, 'script', '//s.swiftypecdn.com/install/v2/st.js', '_st');

_st('install', 'Z4n7msKyctXXfJs66EKx', '2.0.0');
/* eslint-enable func-names, no-param-reassign, no-undef */
