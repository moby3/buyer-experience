title: Suggestions de code assistées par IA de GitLab
og_title: Suggestions de code assistées par IA de GitLab
description: Suggestions de code assistées par IA de GitLab. Conserve votre code source propriétaire en toute sécurité. Vous aide à coder de manière plus productive. Met des milliards de lignes de code à portée de main.
twitter_description: Suggestions de code assistées par IA de GitLab. Conserve votre code source propriétaire en toute sécurité. Vous aide à coder de manière plus productive. Met des milliards de lignes de code à portée de main.
og_description: Suggestions de code assistées par IA de GitLab. Conserve votre code source propriétaire en toute sécurité. Vous aide à coder de manière plus productive. Met des milliards de lignes de code à portée de main.
og_image: /nuxt-images/solutions/code-suggestions/secure-code.jpeg
twitter_image: /nuxt-images/solutions/code-suggestions/secure-code.jpeg
hero: 
  note: Suggestions de code assistées par IA de GitLab
  header: (bêta)
  description: 
    - typed:
        - Conservez votre code source propriétaire en toute
      is_description_inline: true
      highlighted: sécurité
    - typed:
        - Vous aide à coder de manière
      highlighted: plus productive
    - typed:
        - Met des milliards de lignes de code
      highlighted: à portée de main
  image: 
    src: /nuxt-images/solutions/ai/GitLab Logo.png
    alt: Logo d'IA
  button:
    text: Essayer des suggestions de code gratuitement
    href: https://gitlab.com/-/trial_registrations/new?glm_source=localhost/solutions/ai/&glm_content=default-saas-trial
    variant: secondary
copyBlocks:
  blocks: 
    - title: Accélérer le codage
      description: Avec l'aide de l'IA générative qui suggère du code pendant que vous développez, nous aidons les équipes à créer des logiciels plus rapidement et plus efficacement. Complétez une ligne entière de code en appuyant sur une seule touche, démarrez rapidement une fonction, remplissez du code standard, générez des tests et bien plus encore.
    - title: Protéger votre code source
      description: La confidentialité est la base essentielle des suggestions de code. Elle conserve votre code source propriétaire en toute sécurité dans l'infrastructure Cloud d'entreprise de GitLab. Votre code n'est pas utilisé comme données de formation. L'inférence du code source par rapport au modèle de suggestions de code n'est pas conservée. [Découvrez-en plus sur l'utilisation des données](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#code-suggestions-data-usage){data-ga-name="data usage code suggestions" data-ga-location="body"} lors de l'utilisation des suggestions de code.
      img:
        src: /nuxt-images/solutions/code-suggestions/secure-code.svg
        alt: logo code de sécurité
    - title: Des milliards de lignes de code à portée de main
      description: Les suggestions de code utilisent des modèles préformés open source qui sont continuellement affinés avec un ensemble de données open source personnalisé pour permettre une prise en charge multilingue et des cas d'utilisation supplémentaires.
      languageSelected: 'go'
    - title: Assistance dans la langue de votre choix
      description: 'Des suggestions de code basées sur l''IA qui correspondent à votre façon de travailler. L''assistance est disponible dans 13 langues : C/C++, C#, Go, Java, JavaScript, Python, PHP, Ruby, Rust, Scala, Kotlin, et TypeScript '
      logos: [
        "/nuxt-images/logos/c-logo.svg",
        "/nuxt-images/logos/cpp-logo.svg",
        "/nuxt-images/logos/c-sharp-logo.svg",
        "/nuxt-images/logos/javascript-logo.svg",
        "/nuxt-images/logos/typescript-logo.svg",
        "/nuxt-images/logos/python-logo.svg",
        "/nuxt-images/logos/java-logo.svg",
        "/nuxt-images/logos/ruby-logo.svg",
        "/nuxt-images/logos/rust-logo.svg",
        "/nuxt-images/logos/scala-logo.svg",
        "/nuxt-images/logos/kotlin-logo.svg",
        "/nuxt-images/logos/go-logo.svg",
        "/nuxt-images/logos/php-logo.svg",
      ]
resources:
    data:
      title: Nouveautés de GitLab AI
      column_size: 4
      cards:
        - icon:
            name: blog
            variant: marketing
            alt: Icône de blog
          event_type: Blog
          header: Séries sur l'IA et le ML en DevSecOps
          link_text: En savoir plus
          image: /nuxt-images/blogimages/ai-ml-in-devsecops-blog-series.png
          href: /blog/2023/04/24/ai-ml-in-devsecops-series/
          data_ga_name: Séries sur l'IA et le ML en DevSecOps
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: Icône de blog
          event_type: Blog
          header: >-
            GitLab détaille les fonctionnalités assistées par l'IA dans la plateforme DevSecOps
          link_text: En savoir plus
          href: https://about.gitlab.com/blog/2023/05/03/gitlab-ai-assisted-features/
          image: /nuxt-images/blogimages/ai-fireside-chat.png
          data_ga_name: GitLab détaille les fonctionnalités assistées par l'IA dans la plateforme DevSecOps
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: Icône de blog
          event_type: Blog
          header: Comment les suggestions de code assistées par l'IA feront progresser DevSecOps
          link_text: En savoir plus
          href: https://about.gitlab.com/blog/2023/03/23/ai-assisted-code-suggestions/
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          data_ga_name: Comment les suggestions de code assistées par l'IA feront progresser DevSecOps
          data_ga_location: body
ctaBlock:
    title: Quelle est la prochaine étape pour votre programmeur de paires d'IA ?
    cards:
      - header: Suggestions de code pour les instances GitLab auto-géré
        description: |
          Les suggestions de code seront bientôt disponibles pour les instances GitLab auto-géré via une connexion sécurisée à GitLab.com. Si vous avez des exigences particulières pour vos instances GitLab auto-géré, nous vous invitons à exprimer votre intérêt dans notre [ticket d'assistance dédié à GitLab auto-géré](https://gitlab.com/gitlab-org/gitlab/-/issues/409183).
        icon: ai-code-suggestions
      - header: Expérience utilisateur améliorée
        description: | 
          Nous travaillons sur une prise en charge supplémentaire des IDE, notamment des IDE basés sur JetBrains IntelliJ ainsi que sur la prise en charge de Visual Studio pour les suggestions de code. En outre, nous planifions d'améliorer l'expérience des utilisateurs en travaillant sur la façon dont les suggestions sont présentées et acceptées dans les IDE, en donnant aux développeurs un meilleur contrôle sur le fonctionnement de la fonctionnalité.
        icon: user-laptop