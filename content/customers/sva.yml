---
  data:
    title: SVA
    description: SVA uses GitLab for collaboration, version control, test automation, and CI/CD
    og_title: SVA
    twitter_description: SVA uses GitLab for collaboration, version control, test automation, and CI/CD
    og_description: SVA uses GitLab for collaboration, version control, test automation, and CI/CD
    og_image: /nuxt-images/blogimages/sva.jpg
    twitter_image: /nuxt-images/blogimages/sva.jpg
    customer: SVA
    customer_logo: /nuxt-images/logos/sva.svg
    heading: How SVA enhanced business agility workflow with GitLab
    key_benefits:
      - label: Single source of truth
        icon: continuous-delivery
      - label: Improved collaboration
        icon: collaboration-alt-4
      - label: Improved source code management
        icon: code
    header_image: /nuxt-images/blogimages/sva.jpg
    customer_industry: Technology
    customer_employee_count: 1,400
    customer_location: Wiesbaden, Germany
    customer_solution: |
      [GitLab Free](/pricing/){data-ga-name="free solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: GitLab users
        stat: 510
      - label: GitLab groups
        stat: 110
      - label: projects using GitLab
        stat: 1,571
    blurb: SVA was looking for a platform to improve central code management and workflow CI.
    introduction: |
      SVA's application and infrastructure development teams adopted GitLab for internal and external source code management (SCM), DevOps, and continuous integration and delivery (CI/CD).
    quotes:
      - text: |
          GitLab is easy to use. Our system engineers onboarded very fast. You don't have to explain every feature. It is very intuitive.
        author: Stefan Gärtner
        author_role: Head of Competence Center CICD
        author_company: SVA
    content:
      - title: Systems integration leader
        description: |
          System Vertrieb Alexander GmbH (SVA) is a leading systems integration company founded in 1997. Based in Germany, [SVA](https://www.sva.de/index.html) provides a combination of quality IT products with consultation know-how for custom solutions for all industries. The company offers expertise in data center infrastructure, IT platforms, virtualization technologies, software development, applications in general and business intelligence.
      - title: Improving CI and central code management
        description: |
          SVA has different lines of business, and all vendors and services are divided into nine distinct areas. The focus of one department is Agile IT and software development, which includes DevOps and CI/CD.

          The applications and infrastructure development teams were looking for a solution that could improve their CI workflow and help them organize and manage code. SVA was looking for a quickly accessible tool to boost collaboration and code quality for their system engineers.
      - title: Collaboration, transparency, open source community
        description: |
          SVA uses Jira for issues and Kanban boards in their workflow and both continue to be extremely useful. The team reviewed different products and after some analysis, the team found that GitLab was the best option for their particular use case. Jira and GitLab integrate easily, so SVA adopted GitLab's Community Edition for their field engineers.

          Several SVA system engineers had previous experience using GitLab and were familiar with its features. Many SVA customers have also used GitLab and the engineers now implement it into customer environments. Now, the teams also use GitLab to [deploy Kubernetes](/solutions/kubernetes/) clusters.

          "If one team wants to look at what the other team is doing or wants to reuse some Ansible playbooks or some code snippets or anything, this is the central point of collecting these things. It's also where they can develop and improve code," said Sarah Mueck, Head of Business Line Agile IT and Software Development at SVA.
      - title: Internal and external knowledge
        description: |
          SVA has over 500 active GitLab users. Every engineer is now connected to others in an open source environment. The teams use GitLab as a version control system and also for test automation. "We try to make everything public or most things public. We try to connect every system engineer to each other, like an open source community," said Stefan Gärtner, Head of Competence Center CICD at SVA.

          System engineers use [GitLab CI](https://about.gitlab.com/solutions/continuous-integration/) for most of their reports and projects. On top of that, GitLab is used in areas that the team hadn't expected. "We don't use it only as a version control system. I think it's a benefit of GitLab that we are doing test automation now more than before," Gärtner added.

          As a consultant company, SVA needs to be able to adapt to the customer environment. By adopting GitLab internally, consultants now have a broader understanding of how their customers work. "That's why we also adopted GitLab internally. Not just for the purpose of what it's used for, but also as a learning curve for our employees that they use it and know how to use it. So if they come to a customer who's using this as well, then they know what they're doing," Mueck added.

          GitLab mirrors the technical company structure, and teams work together within specific projects or repositories. Cross-group work can happen easily within the tool because of the level of transparency. Collaboration happens easier now with one centralized tool in place.
