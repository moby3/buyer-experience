---
  title: Release
  description: Learn how GitLab&#39;s integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers. View more here!
  components:
    - name: sdl-cta
      data:
        title: Release
        aos_animation: fade-down
        aos_duration: 500
        subtitle: GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers.
        text: |
          GitLab helps automate the release and delivery of applications, shortening the delivery lifecycle, streamlining manual processes, and accelerating team velocity. With zero-touch Continuous Delivery (CD) built right into the pipeline, deployments can be automated to multiple environments like staging and production, and the system just knows what to do without being told - even for more advanced patterns like canary deployments. With feature flags, built-in auditing/traceability, on-demand environments, and GitLab pages for static content delivery, you'll be able to deliver faster and with more confidence than ever before.
        icon:
          name: release-alt-2
          alt: Release Spotlight Icon
          variant: marketing
    - name: featured-media
      data:
        column_size: 6
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        media:
          - title: Continuous Delivery
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Deliver your changes to production with zero-touch software delivery; focus on building great software and allow GitLab CD to bring your release through your path to production for you.
            link:
              href: /solutions/continuous-integration/
              text: Learn More
              data_ga_name: Continuous Delivery
              data_ga_location: body
          - title: Pages
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Use any static site generator to create websites that are easily managed and deployed by GitLab.
            link:
              href: /stages-devops-lifecycle/pages/
              text: Learn More
              data_ga_name: Pages
              data_ga_location: body
          - title: Advanced Deployments
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Mitigate the risk of production deploys by deploying new production code to a small subset of your fleet and then incrementally adding more.
            link:
              href: https://docs.gitlab.com/ee/topics/autodevops/index.html#incremental-rollout-to-production-premium
              text: Learn More
              data_ga_name: Advanced Deployments
              data_ga_location: body
          - title: Feature Flags
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Feature flags enable teams to achieve CD by letting them deploy dark features to production as smaller batches for controlled testing, separating feature delivery from customer launch, and removing risk from delivery.
            link:
              href: https://docs.gitlab.com/ee/operations/feature_flags.html
              text: Learn More
              data_ga_name: Feature Flags
              data_ga_location: body
          - title: Release Evidence
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |

              Release Evidence includes all the assurances and evidence collection that are necessary for you to trust the changes you're delivering.
            link:
              href: https://docs.gitlab.com/ee/user/project/releases/#release-evidence
              text: Learn More
              data_ga_name: Release Evidence
              data_ga_location: body
          - title: Release Orchestration
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Management and orchestration of releases-as-code built on intelligent notifications, scheduling of delivery and shared resources, blackout periods, relationships, parallelization, and sequencing, as well as support for integrating manual processes and interventions.
            link:
              href: https://docs.gitlab.com/ee/user/project/releases/
              text: Learn More
              data_ga_name: Release Orchestration
              data_ga_location: body
          - title: Environment Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Enable organizations to operate and manage multiple environments directly from GitLab. Environments are encapsulated in GitLab as a target system with associated configurations. By facilitating access control, visualizing deployments and deployment history across teams and projects, adding the ability to query environments, and ensuring that environment's configurations are traceable, platform engineers can enact stronger controls and avoid costly mistakes in deployments.
            link:
              href: https://docs.gitlab.com/ee/ci/environments/
              text: Learn More
              data_ga_name: Environment Management
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/release/){data-ga-name="release direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Configure
            icon:
              name: configure-alt-2
              alt: Configure Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 500
            text: |
              Configure your applications and infrastructure.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/configure/
              data_ga_name: Configure
              data_ga_location: body
          - title: Verify
            icon:
              name: verify-alt-2
              alt: Verify Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1000
            text: |
              Keep strict quality standards for production code with automatic testing and reporting.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/verify/
              data_ga_name: verify
              data_ga_location: body
          - title: Package
            icon:
              name: package-alt-2
              alt: Package Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1500
            text: |
              Create a consistent and dependable software supply chain with built-in package management.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/package/
              data_ga_name: package
              data_ga_location: body
