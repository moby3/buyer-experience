---
  title: Drupal Association
  description: Learn how @drupalassoc enabled contribution and collaboration with GitLab
  image_title: /nuxt-images/blogimages/drupalassoc_cover.jpg
  image_alt: Drupal Association eases entry for new committers, speeds implementations
  twitter_image: /nuxt-images/blogimages/drupalassoc_cover.jpg
  data:
    customer: Drupal Association
    customer_logo: /nuxt-images/logos/drupalassoclogo.png
    heading: Drupal Association eases entry for new committers, speeds implementations
    key_benefits:
      - label: Faster implementations
        icon: speed-alt
      - label: Greater user involvement
        icon: user-group
      - label: Enhanced collaboration
        icon: collaboration-alt-4
    header_image: /nuxt-images/blogimages/drupalassoc_cover.jpg
    customer_industry: Software
    customer_active_contributors: 120,000+
    customer_location: Drupal.org
    customer_solution: |
      [GitLab SaaS Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: faster CI implementation
        stat: 3x
      - label: increase in Drupal core contributions
        stat: 10%
    blurb: Implementation of GitLab placed Drupal platform development in the mainstream of open-source evolution, while better supporting the contribution efforts of a wider range of the Drupal user community.
    introduction: |
      Adopting GitLab as The DevOps Platform enabled greater contribution and collaboration.
    quotes:
      - text: |
          You do not need any tools other than your browser in order to contribute, and not just in our module ecosystem, but even to Drupal core itself. That was not possible in our project prior to the move to GitLab, and that's a really big deal.
        author: Timothy Lehnen
        author_role: CTO
        author_company: Drupal Association
    content:
      - title: Drupal evolves with open source to grow its platform
        description: |
          The Drupal Association, steward of the free Drupal web content management system, stands as one of the most remarkable success stories of the open source software era. Drupal serves as the capable backend framework for an estimated 13% of the top 10,000 websites worldwide, with users including the Fortune 50 companies, government, higher education and many more. From website development to advanced content management and more, the platform has evolved, while relying on the innovation, know-how, and dedication of open source contributors. As of early 2021, the Drupal community included 121,000 active contributors. The Drupal Association enables thousands of websites to upgrade to use innovative and stable modules at an increasingly rapid pace.
      - title: Moving forward, reducing obstacles for new contributors
        description: |
          Drupal had its 20th anniversary in 2021. As a veteran open source project, it actually predates git, let alone hosted collaboration platforms like GitLab or GitHub, or continuous integration (CI) services like Travis. Like many of the projects of this era, Drupal built its own bespoke git backend, bespoke code collaboration tools, and bespoke continuous integration systems. Well into 2020, the Drupal ecosystem still primarily relied on patches submitted to Drupal.org’s custom issue tracker, and the bespoke DrupalCI system for integration testing. At the same time, Drupal as a product evolved in complexity and capability — making it an industry-leader in building ambitious digital experiences — but exposing the flaws and friction points of the aging collaboration tools.

          The Drupal.org development team realized that, for Drupal to continue to thrive as an open source platform, they needed to increase focus on their beginner experience, facilitating the continual growth of an essential and vibrant contributor community. New contributors needed to be able to quickly take part in frictionless development efforts. Drupal also needed end-to-end, browser-based management to give new users the ability to manage code contributions and workflow — and to empower users around the globe without access to powerful machines the tools they need to continue to contribute. Achieving these goals was essential to putting the focus back on what makes the Drupal project successful: empowered and energized contributors, focused on innovation, and free of friction.
      - title: GitLab opens up access and collaboration for more developers
        description: |
          To achieve their new goals, the team looked to leverage an open collaboration and DevOps platform that would offer [end-to-end support](/solutions/devops-platform/){data-ga-name="end-to-end support" data-ga-location="customers content"}. The Drupal.org engineering team was faced with the choice of continuing to maintain custom collaboration tools, or moving to a well supported, modern solution. During a multi-year evaluation process, the team evaluated GitLab, GitHub, and Bitbucket, and ultimately went with GitLab as the collaboration and DevOps platform of choice to support their open source project because it offered “that modernized, feature-rich contribution workflow that developers expect, as well as allowing us to run our own self-hosted instance, which was crucial to configuring our new tools to meet Drupal’s very open collaboration style,” noted Timothy Lehnen, Chief Technology Officer at the Drupal Association.

          “After conducting an independent evaluation of multiple collaboration platforms including Github, Atlassian BitBucket, and GitLab, GitLab emerged as a clear winner do to its commitment to co-operation and collaboration with the Drupal project and the [open source community](/solutions/open-source/){data-ga-name="open source" data-ga-location="customers content"},” continued Lehnen. Adopting GitLab also provided a ready leg-up for the many contributors already familiar with git, and that enabled them to recruit more people into the Drupal Community. This modernization also allowed the team to benefit from add-ons created by the ecosystem of tooling providers who extend GitLab’s functionality, including Drupal service providers who had already adopted GitLab for their own internal work.
          
          “With GitLab, Drupal contributors have the tools they need, without the barrier of learning a full development stack that isn’t actually relevant to their domain,” said Lehnen. “It just makes it easier to collaborate on projects both for coders and non-coders.” Community members are now able to employ a browser interface that supports the full end-to-end process of making a contribution. [GitLab CI](/solutions/continuous-integration/){data-ga-name="continuous integration" data-ga-location="customers content"} supports a range of new workflows for the Drupal project. Both coders and non-coders alike can use GitLab’s WebIDE to support writing better code comments and in-code doc-blocks. A variety of users, including non-coders, such as product managers, accessibility engineers, content editors, and project managers, are now integrated into the overall workflow. This integration between coders and non-coders enables unprecedented collaboration across Drupal.
          
          Use of GitLab also coincides with the project’s first implementations of Kubernetes clusters, as GitLab CI runners are being made available in beta to select projects in the Drupal ecosystem, and soon in wider release. This helps the Drupal project move from a centralized CI model to one where project maintainers can implement their CI workflows of choice, innovating faster. As one example, Drupal is expanding beyond just a php project, and now publishes several JavaScript components to NPM, via GitLab CI/Pipelines.
      - title: Enhanced efficiency and expanded capabilities
        description: |
          Implementation has helped widely grow the capabilities of the Drupal community. GitLab is allowing more Drupal users to be more comfortable submitting fixes against the Drupal core. “We are seeing 400+ merge requests in the Drupal ecosystem per month since the change, and about a 50% close rate within the month,” said Lehnen. Since the transition to merge requests, contribution to Drupal core itself is up by around 10%. Contributor resources now are applied more efficiently as the WebIDE saves contributor community members significant time configuring and setting up jobs. “You can make an end-to-end contribution to Drupal with just your browser. You do not need the command line. You do not need a local development environment,” added Lehnen.

          “Now, contributors don’t need to learn how to be a local Drupal developer in order to review new features for accessibility concerns. The ability to contribute without setting up complex local development environments has made it easier for everyone from our accessibility maintainers, documentation editors, product managers, and others to review and comment on the work of our developers,” said Lehnen.

          Meanwhile, the Drupal core team has come very close to 100% test coverage for the Drupal core, and will be able to expand that coverage to new parts of the codebase much more easily with GitLabCI. They’ve achieved primary goals like increased developer satisfaction for the Drupal contributor ecosystem and dramatically reduced dependence on the internal team for contributor feature development. Contributions have been expanded to include the first JavaScript packages to be added to the mostly PHP project.

          Going forward, further GitLab implementations will expand capabilities of the Drupal credit system, which highlights contributors’ efforts and innovations, providing useful metadata to help better understand how the Drupal community works. “Drupal leads the open source world in measuring and understanding our contribution ecosystem. This is one element of our bespoke tools that we’d love to keep, so we’re thrilled that the GitLab team is supporting the idea of bringing this feature to GitLab. That will let the rest of the open source community benefit from these community health metrics as well,” said Lehnen.
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
        ga_name: all customers
        ga_location: customers stories
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
          ga_location: customers stories
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens/
          ga_location: customers stories
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
          ga_location: customers stories
