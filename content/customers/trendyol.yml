data:
  customer: Trendyol
  customer_logo: /nuxt-images/logos/trendyol_logo_correct.png
  heading: Trendyol boosts developer productivity with GitLab
  key_benefits:
    - label: Toolchain simplification
      icon: cog
    - label: Automated documentation creation
      icon: docs
    - label: Easy AWS integration
      icon: cloud-tick
  header_image: /nuxt-images/blogimages/trendyol_cover_image.jpg
  customer_industry: Ecommerce
  customer_location: Istanbul, Turkey
  customer_employee_count: |
    1,300 developers
  customer_solution: |
      [GitLab Self-Managed Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
  sticky_benefits:
    - stat: 30%
      label: Improvement in developer productivity
    - stat: 60%
      label: Reduction in build times
    - stat: 50%
      label: Faster to launch a new application via a single platform 
  blurb: |
    Trendyol developer teams rely on GitLab to turbocharge software deployment, driving market expansion.
  introduction: |
    Speeding development and deployments with GitLab platform, Trendyol is able to expand business-critical online customer services.
  quotes:
    - text: |
       "GitLab's features allowed us to speed up our production cycle, which accelerated our development and deployment process."
      author: Cenk Civici, Chief Technology Officer, Trendyol
  content:
    - title: Trendyol at forefront of Turkish e-commerce expansion
      description: |
        Headquartered in Istanbul, [Trendyol](https://www.trendyol.com) is the largest e-commerce company in Turkey, operating a research and development (R&D) center, a last-mile delivery system, mobile wallets, and a wide-ranging marketplace that offers everything from food to clothing, electronics, and cosmetics.  As part of its mission, the company exports Turkish products over various platforms to consumers around the world. The company is focused on making its mark by dependably serving more than 30 million shoppers and delivering more than 1 million packages every day. To get all of this done, the company uses natural language processing, machine learning, recommendation systems, and big data. State-of-the-art DevOps processes also are vital to achieving Trendyol's business strategy.
    - title: Adapting to change, tackling inefficiencies
      description: |
        As Trendyol has expanded its stable of services and platforms, its developer teams amassed a diverse and complex DevOps toolchain. This caused obvious inefficiencies. Learning and using multiple tools was time-consuming and ineffective for development teams, even slowing down developer onboarding just as the company needed to expand its developer ranks. It also ultimately slowed deployments. The company's DevOps managers needed to change all of that.


        It was clear that using multiple tools, like BitBucket, Jenkins, and GitLab, couldn't continue. They needed one platform.  Because of compliance issues, it needed to be a CI/CD engine. To make all of this happen, Trendyol selected GitLab Premium for a  container-based, runner architecture, enabling faster and continuous deployments. The platform's simplicity was the primary deciding factor. “We knew we wanted to consolidate tools into a single tool so our development team could spend less time context-switching and learning new platforms, and more time improving our product,” said Cenk Çivici, CTO at Trendyol.
    - title: GitLab enables disaster recovery, high availability
      description: |
        Implementing GitLab Premium with OpenStack and the AWS Cloud Platform has provided Trendyol teams with high availability and disaster recovery. GitLab Premium has allowed software development teams to simplify operations and organize using a single resource. And advanced search capabilities have enabled critical collaboration. Now, as new developers start work at Trendyol, they are not faced with learning multiple tools and repeatedly shifting into different environments, according to Civici. “GitLab included all the processes needed to execute a project within a single platform,” he said, adding that GitLab is a “very mature on-premises solution.”
    - title: Faster development enables critical business strategy
      description: |
        Adopting the GitLab platform has meant a 30% improvement in developer productivity, which has translated into a dramatic market expansion. The developer experience is significantly enhanced with this standardization, providing a combined repository, registry, and CI/CD view for developers, along with reliable operations updates for DevOps managers.  New developer onboarding times have dropped from 10 days to eight days as a result of using a single, end-to-end platform and not a disparate toolchain. This high level of integration means GitLab can operate together with LDAP, JIRA, Slack, and similar applications. And streamlined integration with Kubernetes helps Trendyol teams accelerate the addition of new platform features and updates. “GitLab included all the processes needed to execute a project within a single platform,” said Civici.


        GitLab automation means developers are no longer burdened with many manual documentation tasks, while GitLab advanced search allows them to more easily reuse and share code. The platform also helps them learn from each other. For crucial pipeline configurations, GitLab's simple YAML-based processes ensure repeatable processes, while its container-based runner architecture easily handles 70 or more Kubernetes clusters. GitLab features, such as Service Desk and Issues, provide additional productivity. GitLab's priority support further assures 24/7 assistance for the vital infrastructure that powers production services. Trendyol's DevOps and platform teams now are successfully deploying infrastructure as code, and expanding resources into multiple data centers. And using GitLab, launching a new application has become 50% faster - now taking just one day when it used to take two.


        GitLab has become a key part of how the company creates a robust platform that drives its business strategy to become one of the largest e-commerce companies in the world, offering a full range of customer services.

