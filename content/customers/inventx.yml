---
  title: Inventx
  description: Inventx went from using GitLab strictly for code management to improving customer pipelines, reducing toolchain complexity, and faster bug detection with CI/CD. 
  image_title: /nuxt-images/blogimages/cover_image_inventxag_case_study.jpg
  image_alt: How GitLab decreased deployment times from 2 days to just 5 minutes for Inventx AG
  twitter_image: /nuxt-images/blogimages/cover_image_inventxag_case_study.jpg
  data:
    customer: Inventx
    customer_logo: /nuxt-images/case-study-logos/logo-positiv-gray-invent.png
    heading: How GitLab decreased deployment times from 2 days to just 5 minutes for Inventx AG
    key_benefits:
      - label: One unified workflow
        icon: gitlab-release
      - label: Improved code quality
        icon: devsecops
      - label: Faster releases
        icon: speed-alt
    header_image: /nuxt-images/blogimages/cover_image_inventxag_case_study.jpg
    customer_industry: Technology
    customer_employee_count: 300
    customer_location: Chur, Switzerland
    customer_solution: |
      [GitLab Self-Managed Premium](/pricing/premium/){data-ga-name="premium solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: releases per day. Up from 4 releases per year
        stat: 20
      - label: minute deploys. Down from 2 days.
        stat: 5
      - label: minute bug fixes
        stat: 10-60
    blurb: Inventx simplified pipelines and increased operational efficiency with GitLab CI/CD.
    introduction: |
      Inventx AG was looking for a solution to simplify customer pipelines and reduce toolchain complexity. 
    quotes:
      - text: |
          We're detecting bugs very, very fast and they're also fixed very fast. They can be fixed in maybe 10 minutes to one hour. So bugs are fixed very quickly and also automatically fixed in the production environment. This is a great value.
        author: Louis Baumann
        author_role: DevOps Engineer
        author_company:  Inventx AG
    content:
      - title: Swiss IT services provider for leading financial institutions
        description: |
          Inventx AG is a Swiss IT partner for leading financial and insurance service providers. Inventx provides information technology, consulting, and application management services for Switzerland’s prominent financial and insurance organizations. The independent IT company offers individual solutions, the highest quality security, and data protection for its customers.
      - title: Adopt a better developer workflow
        description: |
          As a multinational corporation, Airbus Intelligence needs tools that can help their team collaborate and work more efficiently across the globe. The Intelligence business wanted to avoid the common challenges of many global companies: distributed teams and disconnected toolchains that cause workflow inefficiency and slow production. An improved workflow that could break through these challenges, make teams more efficient, and foster communication was a high priority. Logan Weber is a software automation engineer at the Web Factory. Finding a better developer workflow was one of his core missions, and the Web Factory’s agility makes the team an ideal testing ground for new tools and technologies. For Logan, it was important that any tools they adopt share a similar dedication to innovation. “We’re in the midst of a digital transformation,” Logan said. “We want to join forces with partners who know what they’re doing and can keep up with us.”

          One of the Web Factory team’s big challenges was that their processes just weren’t efficient enough, which led to delayed releases and lost time in development. Developers could spend at least a full day on the production setup, and too much time was being spent on simple tasks that should have been automated. Developers were frustrated with these manual and lengthy processes that stopped them from focusing on code. With a new tool the Web Factory team also hoped to avoid communication breakdowns between teams. After spending time on the production setup, developers would sometimes realise that the final product didn’t correspond to the initial request, which would then lead to additional efforts. “We’d have to create a bug to modify this error. But it wasn’t a bug, it was just a lack of communication,” Logan explained.

          The Web Factory team tested several tools in the search for the right developer workflow. Because the Web Factory team already used Jira, they decided to test other Atlassian products, such as Bitbucket for version control and Bamboo for CI. Unfortunately, BitBucket and Bamboo didn’t offer a user-friendly experience, and both tools lacked some of the functionalities for their needs. The Web Factory team used Jenkins on old projects, but found it too complicated to maintain. They also wanted to be able to store their deployment script processes as code.
      - title: Simplifying customer complexity
        description: |
          Inventx had experience using GitLab for source code management. In order to simplify workflow, they started using GitLab for CI/CD. “We redesigned the whole pipeline and we're just using GitLab and GitLab runners now. It's all centralized into GitLab and that makes it very easy to troubleshoot. We have a very good overview over all the steps and the customers are also very satisfied with the solution. This really helped us to simplify this task,” said Louis Baumann, DevOps engineer.
        
          
          Inventx created a package service for customers. This is a container platform that includes storage, monitoring, logging services, with the whole CI/CD construct running GitLab. The teams no longer need several physical servers to run pipelines. GitLab has allowed them to minimize their toolchain workflow and context switching between tools. “Using GitLab has reduced complexity enormously, and we accelerated  all our workflow processes. It offers us a great overview of all the processes running for one product. And it simplifies the whole toolchain,” according to Baumann. 
      - title: Faster builds, increased deployments
        description: |
          With GitLab, developers are now able to own their own code. “GitLab enables our developers to directly deploy to test environments, to run integration tests on our test environment automatically, and also to deploy some nightly builds to our test environment and test them there, automatically,” Baumann said.
        
          
          Developer production has increased, due to being able to release code on their own. According to Baumann, the team previously was releasing  about four times a year. Now, they’re releasing approximately 20 times a day. “Everything is now contained in GitLab, in one centralized place and  there is one pipeline running where the customer has a great overview of all the different pipelines that were executed,” Baumann said. “If a shot fails, [the customer] can retry at the pipeline, rerun, and has all locks collected on one central project. This is really, really a great value for us and for our customer.”      
        
          
          Engineers are developing on their local machines. Most of the code is written in .NET Core, but they’re starting to write some features in Golang. Teams are using Visual Studio Code as ID, and are deploying using GitLab pipelines to the test environment or the environment from the local machines. The teams execute GitLab runner on Kubernetes; and build their own deployment containers; containing all the functionalities to deploy and manage their resources on Kubernetes.
        
          
          Engineers are able to detect bugs earlier in the software lifecycle because they’re deploying smaller releases, faster. “We're detecting bugs very, very fast and they're also fixed very fast. They can be fixed in maybe 10 minutes to one hour. So bugs are fixed very quickly and also automatically fixed in the production environment. This is a great value,” Baumann added. 
        
          
          GitLab is allowing the teams to have more control, broader visibility, better ability to plan with improved governance. With less tools in the pipelines, customers now have streamlined services.
    customer_success_stories:
      title: Customer success stories
      link:
        text: See all stories
        href: /customers/
        ga_name: all customers
        ga_location: customers stories
      read_story: Read story
      stories:
        - title: Goldman Sachs
          image: /nuxt-images/customers/goldman_sachs_case_study.png
          description: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
          link: /customers/goldman-sachs/
          ga_name: goldman sachs
          ga_location: customers stories
        - title: Siemens
          image: /nuxt-images/customers/siemens.png
          description: How Siemens created an open source DevOps culture with GitLab
          link: /customers/siemens/
          ga_name: siemens
          ga_location: customers stories
        - title: Fanatics
          image: /nuxt-images/customers/fanatics_case_study.png
          description: Fanatics' successful GitLab CI transition empowers innovation cycles and speed
          link: /customers/fanatics/
          ga_name: fanatics
          ga_location: customers stories
