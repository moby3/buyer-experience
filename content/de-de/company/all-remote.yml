---
  title: All-Remote
  description: "GitLab ist eines der größten All-Remote-Unternehmen der Welt"
  call_to_action:
    title:  Ein weltweit führendes Unternehmen für Remote-Arbeit
    shadow: true
    image_border: true
    subtitle: Erfahre, wie wir es geschafft haben, als asynchrones Unternehmen ohne Büro zu skalieren, das immer wieder als großartiger Arbeitsplatz anerkannt wird.
    free_trial_button:
      text: Hol dir das Remote-Playbook
      url: https://learn.gitlab.com/allremote/remote-playbook
      data_ga_name: Get the remote playbook
      data_ga_location: header
    contact_sales_button:
      text: TeamOps-Kurs
      url: /teamops/
      data_ga_name: TeamOps — making teamwork an objective discipline
      data_ga_location: header
    image:
      image_url: /nuxt-images/all-remote/remote-playbook-2023.png
      alt: Link zum Remote-Playbook 2022
      image_href: https://learn.gitlab.com/allremote/remote-playbook
  feature_list:
    title: "Der umfassende Leitfaden für Remote-Arbeit"
    subtitle: "GitLab arbeitet seit seiner Gründung ausschließlich aus der Ferne und wendet den DevSecOps-Plattform-Ansatz an, um die Art und Weise zu revolutionieren, wie Menschen in über 65 Ländern zusammenarbeiten, kommunizieren und Ergebnisse erzielen. Unser Wissen und unsere Best Practices sind für die Öffentlichkeit zugänglich."
    icon:
      name: "remote-work-thin"
      alt: "Remote-Work-Symbol"
      variant: marketing
    features:
      - title: "Remote-work essentials"
        icon:
          name: "remote-world-alt"
          alt: "Remote-Erde-Symbol"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: "Vergleich: All-Remote vs. Hybrid-Remote"
            url: /company/culture/all-remote/all-remote-vs-hybrid-remote-comparison/
            data_ga_location: 'body'
            data_ga_name: "Compare: All-Remote vs. Hybrid-Remote"
          - title: "Wie man ein Unternehmen auf Remote-Arbeit umstellt"
            url: /company/culture/all-remote/transition/
            data_ga_location: 'body'
            data_ga_name: "company transition to remote operation"
          - title: "Was man bei der Einführung von Remote-Arbeit vermeiden sollte"
            url: /company/culture/all-remote/what-not-to-do/
            data_ga_location: 'body'
            data_ga_name: "What not to do when implementing remote work"
      - title: "Gestaltung eines Remote-Teams"
        icon:
          name: "remote-chat"
          alt: "Remote-Welt-Symbol"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: Über die Rolle des Head of Remote
            url: /company/culture/all-remote/head-of-remote/
          - title: Die 10 Modelle von Remote-Teams
            url: /company/culture/all-remote/stages/
          - title: Die Bedeutung von Dokumentation für Remote-Teams
            url: /company/culture/all-remote/handbook-first-documentation/
      - title: Remote-Rekrutierung & -Onboarding
        icon:
          name: "user-laptop"
          alt: "Remote-Benutzer-Laptop-Symbol"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: Wie man ein Remote-Team rekrutiert
            url: /company/culture/all-remote/hiring/
          - title: Der Leitfaden für Remote-Onboarding
            url: /company/culture/all-remote/onboarding/
          - title: Wie man ein Remote-Jobangebot evaluiert
            url: /company/culture/all-remote/evaluate/
      - title: Zusammenarbeit & Async
        icon:
          name: "idea-collaboration"
          alt: "Symbol für Remote-Ideen-Zusammenarbeit"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: Wie man asynchron arbeitet
            url: /company/culture/all-remote/asynchronous/
          - title: Wie man Meetings effektiv aus der Ferne führt
            url: /company/culture/all-remote/meetings/
          - title: Remote-Zusammenarbeit und Whiteboarding
            url: /company/culture/all-remote/collaboration-and-whiteboarding/
      - title: Skalierung der Remote-Kultur
        icon:
          name: "collaboration"
          alt: "Zusammenarbeitsymbol"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: Wie man eine Kultur mit Remote-Teams aufbaut
            url: /company/culture/all-remote/building-culture/
          - title: Die Bedeutung der informellen Kommunikation
            url: /company/culture/all-remote/informal-communication/
          - title: Aufbau einer integrativen Remote-Kultur
            url: /company/culture/inclusion/building-diversity-and-inclusion/
      - title: Mehr erfahren
        icon:
          name: "handbook-gitlab"
          alt: "Remote-Handbuch-Symbol"
          variant: marketing
        description: "Ausführliche Informationen über alles, was du über Remote-Arbeit wissen musst, findest du im GitLab-Leitfaden für Remote-Arbeit."
        remove_bullet_points: true
        subfeatures:
          - title: Lies den vollständigen Remote-Leitfaden von GitLab
            url: /company/culture/all-remote/guide/
  highlights_header: Fallstudien zur Fernarbeit
  highlights_1:
    font_variant: 'lg'
    customers:
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-1.png"
          alt: ""
        url: "https://store.hbr.org/product/gitlab-and-the-future-of-all-remote-work-a/620066"
        impact: "GitLab und die Zukunft der Remote-Arbeit"
        logo: "/nuxt-images/all-remote/harvard_business_review_logo.svg"
        logo_alt: Harvard-Business-Review-Logo
        stats:
          - qualifier: "Wertschöpfung durch das All-Remote-Modell"
          - qualifier: "Organisatorische Prozesse in großem Maßstab"
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-2.png"
          alt: ""
        logo: "/nuxt-images/all-remote/remote-logo-white.svg"
        logo_alt: Remote-Logo
        url: "/customers/remote/"
        name: "Remote"
        impact: "100 % der Termine mit GitLab einhalten."
        stats:
          - qualifier: "Eine einzige Quelle der Wahrheit"
          - qualifier: "Kein Kontextwechsel"
  highlights_2:
    font_variant: 'lg'
    customers:
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-3.png"
          alt: ""
        logo: "/nuxt-images/all-remote/insead.svg"
        logo_alt: Insead-Logo
        url: "https://publishing.insead.edu/case/gitlab"
        impact: "GitLab: Kann „All-Remote“ skaliert werden?"
        stats:
          - qualifier: "Beispiele für asynchrone Arbeit"
          - qualifier: "Diskussion zum organisatorischen Aufbau"
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-4.png"
          alt: ""
        logo: "/nuxt-images/all-remote/hotjar_logo.svg"
        logo_alt: Hotjar-Logo
        url: "/customers/hotjar/"
        impact: "Das vollständig remote arbeitende Hotjar-Team kann mit GitLab doppelt so schnell bereitstellen."
        stats:
          - qualifier: "End-to-End-Transparenz"
          - qualifier: "All-Remote-Betrieb"
  events:
    hide_horizontal_rule: true
    icon:
      name: ribbon-check-alt-thin
      alt: Orden-mit-Häkchen-Symbol
      variant: marketing
    header: Erhalte ein Zertifikat für Remote-Arbeit
    cards_per_row: 2
    description: Werde zum Experten für die wichtigsten Fähigkeiten, um an einem verteilten Arbeitsumfeld erfolgreich zu sein.
    events:
      - heading: Wie man ein Remote-Team führt
        event_type: Kurs
        destination_url: 'https://www.coursera.org/learn/remote-team-management'
        button_text: Veranstaltungsdetails
      - heading: Remote-Grundlagentraining
        event_type: Schulung
        destination_url: https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge
        button_text: Veranstaltungsdetails
  resources_tabs:
    title: Related Resources
    align: left
    column_size: 4
    grouped: true
    cards:
      - icon:
          name: blog
          variant: marketing
          alt: ''
        event_type: "Blog"
        header: 5 Wege zur Skalierung der Remote-Arbeit in deinem Team
        link_text: Erfahren Sie mehr
        href: /blog/2021/08/09/five-ways-to-scale-remote-work/
        data_ga_name: "5 ways to scale remote work on your team"
        data_ga_location: resource cards
      - icon:
          name: blog
          variant: marketing
          alt: ''
        event_type: "Blog"
        header: Tipps für das Management von Remote-Arbeit in Ingenieurteams
        link_text: Erfahren Sie mehr
        image: "/nuxt-images/resources/resources_1.jpeg"
        href: /blog/2021/01/29/tips-for-managing-engineering-teams-remotely/
        data_ga_name: "tips for managing remote working engineering teams"
        data_ga_location: resource cards
      - icon:
          name: blog
          variant: marketing
          alt: ''
        event_type: "Blog"
        header: Wie wir in den Bereichen UX und Design remote arbeiten
        link_text: Erfahren Sie mehr
        image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
        href: /blog/2020/03/27/designing-in-an-all-remote-company/
        data_ga_name: "how we carry out remote work UX and design"
        data_ga_location: resource cards    
      - icon:
          name: video
          variant: marketing
          alt: ''
        event_type: "Video"
        header: "Warum Remote-Arbeit?"
        link_text: "Schau jetzt"
        image: "/nuxt-images/all-remote/2.jpg"
        href: https://www.youtube-nocookie.com/embed/GKMUs7WXm-E
        data_ga_name: why work remotely
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: ''
        event_type: "Video"
        header: Die Mechanik der Remote-Arbeit
        link_text: "Schau jetzt"
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-webcast-the-mechanics.jpg
        href: https://www.youtube-nocookie.com/embed/9xvVMglCm6I
        data_ga_name: The Mechanics of All Remote
        data_ga_location: resource cards     
      - icon:
          name: video
          variant: marketing
          alt: ''
        event_type: "Video"
        header: "Burnout vorbeugen und Balance finden"
        link_text: "Schau jetzt"
        image: "/nuxt-images/all-remote/vid-thumb-universal-remote-preventing-burnout.jpg"
        href: https://www.youtube-nocookie.com/embed/Q9yjo6IOqX4
        data_ga_name: Preventing Burnout and Achieving Balance
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: ''
        event_type: "Video"
        header: "Remote-Management neu gedacht"
        link_text: "Schau jetzt"
        image: "/nuxt-images/all-remote/vid-thumb-universal-remote-rethink-remote-management.jpg"
        href: https://www.youtube-nocookie.com/embed/dsnSb-lpZSI
        data_ga_name: Rethinking Remote Management
        data_ga_location: resource cards 
      - icon:
          name: video
          variant: marketing
          alt: ''
        event_type: "Video"
        header: "Tipps für eine produktive Remote-Belegschaft"
        link_text: "Schau jetzt"
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-tips-for-a-productive-workforce.jpg
        href: https://www.youtube-nocookie.com/embed/CsLswGz6J5s
        data_ga_name: Tips for a productive all-remote workforce
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: ''
        event_type: "Video"
        header: "Sid und Dominic diskutieren über Remote-Arbeit"
        link_text: "Schau jetzt"
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-ceo-dominic-monn.jpg
        href: https://www.youtube-nocookie.com/embed/EeUhxQn_ct4
        data_ga_name: Sid and Dominic discuss all remote work
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: ''
        event_type: "Video"
        header: "Außergewöhnliche Teams führen"
        link_text: "Schau jetzt"
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-managing-exceptional-teams.jpg
        href: https://www.youtube-nocookie.com/embed/6VVAsMEKqFU
        data_ga_name: Managing Exceptional Teams
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: ''
        event_type: "Video"
        header: "Entwicklung einer Unternehmenskultur, die vollständig auf Remote-Arbeit basiert"
        link_text: "Schau jetzt"
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-fireside-chat.jpg
        href: https://www.youtube-nocookie.com/embed/hVUzXmjKQJ0
        data_ga_name: Evolving a Fully Remote Company Culture
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: ''
        event_type: "Video"
        header: "Video für eine effektive Zusammenarbeit nutzen"
        link_text: Schau jetzt
        image: /nuxt-images/all-remote/vid-thumb-universal-using-video-collaboration.jpg
        href: https://www.youtube-nocookie.com/embed/6mZqzK_40FE
        data_ga_name: Using video for effective collaboration
        data_ga_location: resource cards
      - icon:
          name: report
          variant: marketing
          alt: ''
        event_type: "Bericht"
        header: "Der Bericht zu Remote-Arbeit"
        link: /
        link_text: Erfahren Sie mehr
        href: /company/culture/all-remote/remote-work-report
        data_ga_name: 'The remote work report'
        data_ga_location: resource cards
      - icon:
          name: report
          variant: marketing
          alt: ''
        event_type: "Bericht"
        header: "Leitfaden für die Arbeit im Home Office"
        link_text: Erfahren Sie mehr
        href: /company/culture/all-remote/work-from-home-field-guide/
        data_ga_name: 'work from home guide'
        data_ga_location: resource cards
      - icon:
          name: report
          variant: marketing
          alt: ''
        event_type: "Bericht"
        header: 'Außerhalb des Büros'
        link_text: Erfahren Sie mehr
        href: /company/culture/all-remote/out-of-the-office
        data_ga_name: 'out of the office'
        data_ga_location: resource cards