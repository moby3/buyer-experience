---
  data:
    title: Dunelm Group PLC
    description: 'Dunelm “shifts left”: U.K. homewares leader moves security to front of cycle, boosts cloud move'
    og_title: Dunelm Group PLC
    twitter_description: 'Dunelm “shifts left”: U.K. homewares leader moves security to front of cycle, boosts cloud move'
    og_description: 'Dunelm “shifts left”: U.K. homewares leader moves security to front of cycle, boosts cloud move'
    og_image: /nuxt-images/customers/dunelm-social-image.jpeg
    twitter_image: /nuxt-images/customers/dunelm-social-image.jpeg

    customer: Dunelm
    customer_logo: /nuxt-images/logos/dunelm.svg
    heading: 'Dunelm “shifts left”: U.K. homewares leader moves security to front of cycle, boosts cloud move'
    key_benefits:
      - label: Increased security
        icon: shield-check-light
      - label: Streamlined collaboration
        icon: collaboration
      - label: Accelerated development
        icon: agile
    header_image: /nuxt-images/blogimages/dunelm.png
    customer_industry: Retail
    customer_employee_count: 3300+
    customer_location: Leicester, U.K.
    customer_solution: |
      [GitLab Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: deployments per week (previously 10-20)
        stat: 75-85
      - label: For onboarding instead of days
        stat: Hours
    blurb: Dunelm Group PLC vigorously pursued fast development and deployment that baked in security at the outset.
    introduction: |
      The retailer chose GitLab SaaS Ultimate to integrate tools and seamlessly deploy secure pipelines on the AWS cloud.
    quotes:
      - text: |
          GitLab can do everything we want it to do, from security, performance, testing, and more.
        author: Chintan Parmar
        author_role: Principal Platform Engineer
        author_company: Dunelm
    content:
      - title: 'CI/CD improvements help drive Dunelm’s tech culture'
        description: |
          Founded in 1979, Dunelm grew into the United Kingdom’s top homewares retailer, with distribution centers, 178 stores, and a robust ecommerce operation. The company sees over 12 million online transactions per year, while maintaining a large online catalog of homewares and home furnishings. More than ever, Dunelm relies on innovative technology engineering to improve customer experiences. 

          This occurs in a retail environment that was dramatically altered by digital transformation. Continuous integration and deployment (CI/CD), assured security, test-driven development, agile sprint-based planning, and advanced DevOps tooling are key parts of Dunelm tech culture. The company’s technology teams are also readily embracing serverless technologies, event-driven architecture, and cloud-first development.
      - title: Retailer looked to build open-source dexterity on stable automation platform
        description: |
          As Dunelm engineering teams accelerated their move to their target architecture of serverless technologies and cloud first, they identified serious gaps in their existing CI/CD tooling. Greater automation, improved governance, security, and agility were necessary in order to integrate a variety of plug-ins and to quickly create resilient software pipelines. Existing workflows — including those based primarily on Jenkins — called for hands-on management and an undue degree of firefighting for any cases of breaking code, and visibility into pipelines was lacking.

          This loomed as a continual and pressing strain on administrative management for Dunelm. Engineering leadership wanted a strategic, stable platform that was scalable. Importantly, better static application security testing (SAST) and dynamic application security testing (DAST) were needed in the face of global cybersecurity threats. Comparative evaluations led Dunelm leadership to identify GitLab CI/CD as the DevOps platform to enable tech teams to “shift left” — that is, to take on performance, testing, and security issues at the beginning of, and throughout, the software development lifecycle, according to Chintan Parmar, Principal Platform Engineer at Dunelm.
      - title: Seamless pipeline deployments to AWS using GitLab
        description: |
          Today, GitLab’s DevOps platform is used to effectively and securely manage builds, integrations, and deployments of Dunelm’s services. “Previously, we built the libraries and functionality in-house for our CI/CD pipelines. If we wanted to do anything new, we had to write this ourselves,” says Parmar. “GitLab can do everything we want it to do, from security, performance, testing, and more. We can build our pipelines in a readable, modular, and consistent fashion.” The platform’s integration capabilities have proved to be particularly useful in creating pipelines on AWS. “Pipelines are deployed seamlessly to AWS using GitLab,” Parmar adds.

          At the same time, the GitLab platform offers other benefits to Dunelm.

          For example, fully onboarding a new developer into Dunelm’s technology stack now may take hours as opposed to days. Furthermore, GitLab’s fully documented workflows get members of multiple teams quickly up to speed on any issues coming out of Dunelm’s pipelines. The GitLab platform facilitates effective collaboration with features such as the merge request process. As a result, developers, quality engineers, site reliability engineers, and others can work in tandem when addressing issues with pipelines. “GitLab’s tech teams have been helpful to engage with us in the implementation of the platform,” says Parmar.
      - title: Teams run more scans, tackle vulnerabilities, collaborate effectively
        description: |
          GitLab effectively supports Dunelm’s objective to “shift left” as part of their DevSecOps strategy. Dunelm teams can run more sophisticated scans more often and in an automated fashion within GitLab pipelines. With SAST/DAST scanning, secret detection, dependency scanning, and more at early stages, security vulnerabilities are captured much earlier in the process, and consequently are remediated much earlier in the software development cycle. The benefits are passed down to customers using Dunelm’s ecommerce platform, because so much security work is done well ahead of software delivery.

          The platform also supports increased numbers of deployments through automation without requiring additional developer and administrator effort. Meanwhile, the GitLab platform has enabled better collaboration between teams, supporting true DevOps partnerships between the different squads and tribes. The software provides visibility into pipeline work that is useful for management of overall operations. This also enables teams to be prepared for code audits. With GitLab Ultimate SaaS, Dunelm was able to manage an open source tool chain using a convenient self-service model. GitLab’s plug-and-play integrations with third-party tools such as Jira, Datadog, Terraform, Slack, and others means teams were no longer “managing blind.”

          “We were looking for a platform that made sure we could build pipelines seamlessly, and also had security built in from the onset,” says Parmar. “That meant the platform aligned with our tech principles — a fast feedback loop, continuous improvement, and delivering working software quickly and safely to our customers.”

          “The GitLab user interface is designed and built to provide an end-to-end stack view. As far as visibility goes, projects are easier to see within GitLab. So, I can see what’s going on much more easily, but if I want, I can still get my hands dirty, and look at what code is being produced. We are generally releasing more quality software with GitLab,” Parmar adds. He also pointed to GitLab’s regularly published technology roadmaps and its monthly release cycle as important factors in making sure Dunelm stays at the forefront of cutting-edge technology.

          Finally, GitLab has contributed to that highly treasured benefit: developer happiness.

          “One thing to point out is that it makes engineering teams happier, which is something we’re always striving to improve,” says Parmar. “When you’re using good tooling and good products, this always helps. If techies love using it, they’re going to work happier, smarter, more efficiently.”
