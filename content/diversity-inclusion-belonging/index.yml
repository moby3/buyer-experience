---
  title: GitLab Diversity, Inclusion, and Belonging
  description: At GitLab, we're building a culture where everyone belongs and everyone can contribute.
  og_title: GitLab Diversity, Inclusion, and Belonging
  twitter_description: At GitLab, we're building a culture where everyone belongs and everyone can contribute.
  og_description: At GitLab, we're building a culture where everyone belongs and everyone can contribute.
  dib_hero:
    header: GitLab Diversity, Inclusion, and Belonging
    primary_btn:
      text: Download the 2023 report
      href: /documents/12170_GitLab_DIB_Presentation_07-25-23.pdf
      data_ga_name: get-the-2023-report
      data_ga_location: 'hero'
      icon: download
    image:
      src: /nuxt-images/dib/hero.png
      alt: 'hero image'
  dib_intro_highlight:
    header: Everyone means
    image:
      src: /nuxt-images/dib/stack-text.png
      alt: 'stack text'
    video:
      image: /nuxt-images/dib/video-placeholder.png
      src: https://player.vimeo.com/video/848558836?h=2fc8efce78
      title: ''
    copy:
      header: Inclusion has always been at the heart of GitLab.
      text: |
            In fact, we founded the company with a mission to make it so that everyone can contribute—and we know that “everyone” is a powerful word. We don't take that lightly.

            “Everyone” is a promise to honor the experiences of each person, their individuality, and their voice. More importantly, it's a commitment to help them harness that individuality, to make their uniqueness essential. That goes for our users and our team members. Because when we can channel each person's experience—that is, when we can add everyone into our product—then we truly become innovators. And by prioritizing Diversity, Inclusion, and Belonging (DIB), we lay the groundwork to multiply our capabilities. 

            To put it simply, we need the contributions of everyone in order to empower our team members, help our users change the world, and transform the industry.
  dib_copy_block_0:
    reverse: true
    sub_header: From our CEO
    highlight: GitLab's mission is to make it so that everyone can contribute. You can't reach everyone unless Diversity, Inclusion, and Belonging (DIB) is core to your company and how you work.
    text: 'DIB has always been a core value of GitLab and reflected in the annual company goals: “Continue to build a diverse team of top talent that we retain and grow.” DIB helps us accelerate innovation, attract top talent, and deepen team member engagement.'
    read_more:
      button_text: Read More
      button_text_open: Read Less
      text: |
        In fiscal year 2023 (February 2022 through January 2023), GitLab exceeded its aspirational, internal goals for hiring and promotion of team members from <a data-ga-location="body" data-ga-name="underrepresented groups" href='https://about.gitlab.com/company/culture/inclusion/#examples-of-select-underrepresented-groups'>underrepresented groups (URGs)</a>, including women in senior leadership and URGs in management positions. I am proud of the progress that the team has made. Building a diverse and inclusive workplace takes time, effort, intentionality, and persistence. And it's an investment we are committed to continue both externally and for our team members, together, one year at a time.
    author:
      name: Sid Sijbrandij
      role: GitLab Co-founder and Chief Executive Officer
    image:
      src: /nuxt-images/dib/sid-sijbrandij.png
      alt: ''
  dib_copy_block_1:
    header: GitLab's DIB vision
    highlight: Diversity, Inclusion and Belonging has been a core value at GitLab since the organization began. Our vision in FY24 is a future where the Diversity, Inclusion, and Belonging value empowers everyone to contribute.
    text: 'When I started at GitLab a little over a year ago, I was encouraged by what existed around Diversity Inclusion and Belonging. We had executive leadership backing our DIB initiatives, a number of Team Member Resource Groups (TMRGs) to support our workforce and Key Performance Indicators (KPI) focused on DIB representation. We also had areas where we could make a bigger difference: working with our leaders on embedding DIB into their divisions in meaningful ways that reflect their accountability to DIB as a value. Reimagining our TMRGs to reflect our global workforce and utilizing them to both engage and retain our team members. We also looked to refocus our efforts on ensuring our workforce is representative of the community and customers we serve at every level of the organization.'
    read_more:
      button_text: Read More
      button_text_open: Read Less
      text: |
        I am encouraged by the advancements we've made in these areas. Throughout this report, we'll share more details by focusing on our workforce, our workplace, how we have made progress through collaborative partnerships, and where we still have work to do.

        DIB at GitLab in FY23 was focused on continuing the mission of diversity, inclusion, and belonging, where, like our company's mission, we believe in a world where everyone can contribute, where everyone has a voice and is empowered to show up as their full selves each and every day.

        Our vision in FY24 is a future where the Diversity, Inclusion, and Belonging value empowers everyone to contribute. We're excited to be highlighting this important work and sharing this report, so you can find out more about GitLab Diversity, Inclusion, and Belonging in practice, our team members' experiences, and where we still have work to do.
    author:
      name: Sherida McMullan
      role: Vice President of Diversity, Inclusion & Belonging
    image:
      src: /nuxt-images/dib/sherida-mc-mullan.png
      alt: 'sherida-mc-mullan'
  dib_workforce:
    header: The people who make us proud
    text: |
      People make our product go; more specifically, unique people make our product go. And we know that because we've seen it work. We've seen how individual contributors change the game, dream up new product features, and breathe life into our culture.

      Giving everyone the power to contribute starts with making our group of team members as representative of everyone as possible. It starts with putting our words into action, and we're proud of who we've become and the team we've put together.
    image:
      src: /nuxt-images/dib/workforce.png
      alt: ''
    tabs:
      - header: How we fulfill our DIB workforce goals
        icon: check-circle-alt
        content: |
          Building a diverse and inclusive workplace is one of our top priorities, and it requires effort and a dedicated effort across the company.  We're committed to that investment, and will continue to improve  year over year.

          **At GitLab we are building an inclusive workplace by:**

          - Beating our aspirational representation goal of 30% senior women in leadership by 7%
          - Increasing our underrepresented group representation across all job grades, exceeding our CTO and CEO aspirational quarterly goals focused on URG management and senior leadership
          - Establishing 3 new <a data-ga-location="body" data-ga-name="team member resource groups" href="https://about.gitlab.com/company/culture/inclusion/erg-guide/#definition-of-the-tmrg---team-member-resource-groups">Team Member Resource Groups (TMRGs)</a> focused on inclusion and belonging: GitLab <a data-ga-location="body" data-ga-name="caregivers" href="https://about.gitlab.com/company/culture/inclusion/tmrg-caregivers/">Caregivers</a>, <a data-ga-location="body" data-ga-name="global voices" href="https://about.gitlab.com/company/culture/inclusion/tmrg-global-voices/">Global Voices</a>, and <a data-ga-location="body" data-ga-name="black@gitlab" href="https://about.gitlab.com/company/culture/inclusion/tmrg-gitlab-black/">Black@GitLab</a>
        quote:
          text: |
            I'm honored to lead the Asian and Pacific Islander TMRG. A few of our recent events were Diwali, Chinese New Year, Holi & more! GitLab's DIB initiatives establish a sense of belonging where team members feel more connected at work, promoting a healthy working environment.”
          author: Sheela Viswanathan
          role: Senior Manager, Sales Systems  and Co-lead API-TMRG
          headshot:
            src: '/nuxt-images/dib/sheela-viswanathan.png'
            alt: ''
      - header: The story our DIB data tells about our people
        icon: user-group
        content: |
          For FY23 (February 2022 to January 2023), our goal was to continue building a diverse team of top talent that we retain and grow. We're proud of the progress we've made in doing that.

          **Here are a few of the stats that indicate our current representation:**
        stats:
          - value: 31%
            blurb: of our workforce is women
          - value: 35%
            blurb: of our management staff are women
          - value: 37%
            blurb: of global senior leadership at GitLab are women
          - value: 17%
            blurb: of our workforce is comprised of URGs
          - value: 13%
            blurb: of our management team is comprised of URGs
          - value: 13%
            blurb: of our senior leadership group is comprised of URGs
          - value: 0.68%
            blurb: of GitLab's management team identifies as non-binary
          - value: 6.6
            blurb: percentage point decrease in regrettable underrepresented group (URG) attrition relative to a year ago.
        legend: You can see all of our team member representation data <a data-ga-location="body" data-ga-name="team member representation" href="https://about.gitlab.com/company/culture/inclusion/identity-data/">here</a>.
  dib_workplace:
    header: Building a workplace where everyone belongs
    text: At GitLab, we're building a culture where everyone belongs and everyone can contribute. Our ability to do that rests upon listening and using every available resource to take action that results in change. When we do that, we create a workplace where everyone feels valued, a place where team members know they'll be accepted and want to come to work.
    image:
      src: '/nuxt-images/dib/workplace2.png'
      alt: ''
    quote:
      text: |
        Many companies claim to encourage professional growth, but GitLab is the first company where I've worked that I feel actually is committed to this promise.”
      author: Yimei Lin
      role: GitLab Senior Account Executive
      headshot:
        src: '/nuxt-images/dib/yimei-lin.png'
        alt: ''
    tabs:
      - header: Our culture of listening
        icon: chat
        content: |
          A big part of GitLab's engagement strategy involves collaboration across many teams and functions. Our centralized People Communications & Engagement team coordinates team member communications, engagement resources, team member listening, and maintaining leadership resources that allow managers at GitLab to lead engaged teams.

          GitLab runs an annual engagement survey and supplements that annual survey with at least one additional survey that broadly gauges team member sentiment. For the past three years, we've taken part in the Great Place to Work survey to understand team member perceptions. In FY23, we achieved an <a data-ga-location="body" data-ga-name="survey-results" href="https://about.gitlab.com/handbook/people-group/engagement/#fy23-q2-pulse-engagement-survey-results">82% participation rate</a> and an overall ‘favorable' engagement score of 81%. We scored 8% higher than our New Tech peer group, which consists of ~150 fast growing and disruptive tech companies with over 1,000 total team members each. These results show that GitLab is an industry leader from an engagement perspective.
        read_more:
          text: |
            As an organization that values transparency, we publish all of our engagement survey results in our public handbook. This allows both team members and prospective candidates to get a view into our culture: <a data-ga-location="body" data-ga-name="culture" href="https://about.gitlab.com/handbook/people-group/engagement/">https://about.gitlab.com/handbook/people-group/engagement/</a>
          button_text: Read More
          button_text_open: Read Less
        quote:
          text: |
            It's very important that we are having important conversations and that we make sure we are all understanding each other's lived experiences.”
          author: Marcus Carter
          role: GitLab Senior Sales Recruiter
          headshot:
            src: '/nuxt-images/dib/marcus-carter.jpg'
            alt: ''
      - header: Our culture of action
        icon: paper-airplane
        content: |
          Many of the elements that have enhanced our DIB culture have come from the insights of our team members, the expansion of our DIB staff, and the never-ending question: what can we do better?
          Here are a few components of GitLab's DIB value that we've put into action.
        accordion:
          - icon:
              name: group
              alt: group Icon
              variant: marketing
            header: Team Member Resource Groups (TMRGs)
            text: |
              TMRGs are voluntary, team member-led groups focused on fostering diversity, inclusion and belonging within GitLab. TMRGs build a sense of belonging through team members: togetherness, camaraderie, and connections. These groups are open to all members of the GitLab community.

              **Within these groups, members can experience:**

              - Reverse AMAs: Ask Me Anything Q&A with our Executive Group and team members.
              - Stay interviews: Small cohort conversations with Executive Group leaders and our underrepresented groups with the goal of gaining valuable insights to ensure we are fostering an inclusive culture where all team members can thrive.
              - Sales sponsorship pilot: A <a data-ga-location="body" data-ga-name="nine-month program" href="https://about.gitlab.com/company/culture/inclusion/DIB-Sponsorship/">nine-month program</a> that paired our senior CRO leaders with underrepresented team members to help create pathways, visibility, and advocacy to assist with progression within Sales.
              - <a data-ga-location="body" data-ga-name="tmrg leadership recognition program" href="https://about.gitlab.com/company/culture/inclusion/erg-guide/#roles-within-the-group">TMRG Leadership Recognition Program:</a> A bonus provided to team members leading our TMRGs or DIB-focused groups to recognize great work being done to support the DIB strategy and DIB Team and embedding the DIB value at GitLab
              - <a data-ga-location="body" data-ga-name="dib speaker series" href="https://about.gitlab.com/company/culture/inclusion/DIB-Speaker-Series/">DIB Speaker Series</a>: These are speakers, panels, or workshops with leaders internally and externally from a variety of communities. These events provide educational opportunities and inspire allyship with underrepresented groups.
              - <a data-ga-location="body" data-ga-name="executive (vp+) sponsorship" href="https://about.gitlab.com/company/culture/inclusion/erg-guide/#executive-sponsorship">Executive (VP+) sponsorship</a>: The role is to support the development of the TMRG, provide advocacy at the leadership level, connect the TMRG to GitLab's mission and goals, and provide advocacy to TMRG leaders.
              - TMRG community Slack spaces: TMRGs create safe spaces to connect.
            quote:
              text: |
                As the Executive Sponsor of the Black@GitLab TMRG, I advocate for the team member group and make sure that it's happening at the executive level as well as across the company. My goal is also to provide visibility into what's happening so members of the TMRG can be truly connected to the company.”
              author: David DeSanto
              role: GitLab Chief Product Officer
              headshot:
                src: '/nuxt-images/dib/david-de-santo.png'
                alt: ''
          - icon:
              name: money
              alt: money Icon
              variant: marketing
            header: Sponsorships
            text: |
              We've carefully selected and partnered with organizations that enhance and promote our DIB practice both internally and publicly. These sponsorships afford us additional resources to magnify our culture for existing team members and raise awareness for our DIB emphasis in our talent pipeline.

              **A few of our sponsorships include:**
                - <a data-ga-location="body" data-ga-name="black is tech 202" href="https://blackistechconference.com/">Black is Tech 2022</a>
                - <a data-ga-location="body" data-ga-name="dev/color in motion 2022" href="https://www.devcolorinmotion.com/">Dev/Color in Motion 2022,</a> San Francisco
                - <a data-ga-location="body" data-ga-name="girl develop it career fair 202" href="https://girldevelopit.com/events/details/girl-develop-it-career-conversations-presents-virtual-career-fair/">Girl Develop It Career Fair 2022</a>
                - <a data-ga-location="body" data-ga-name="rails girls warsaw 2022" href="https://railsgirls.com/warsaw.html">Rails Girls Warsaw 2022</a>, Poland
                - Speaker Engagements at:

                  - <a data-ga-location="body" data-ga-name="venturefizz career progression panel 2022" href="https://venturefizz.com/videos/venturefizz-event-video-which-stage-company-fit-your-career-public-company-edition">VentureFizz Career Progression Panel 2022</a>
                  - Women in Sales Everywhere (WISE) - Making the Most of Mentorship
                  - WISE - Burnout: What is it and how do we manage it?
                  - WISE - EOY Reflections: Preparing for 2023

          - icon:
              name: collaboration-alt-4
              alt: collaboration Icon
              hex_color: "#171321"
              variant: marketing
            header: Leadership Council
            text: |
              Our <a data-ga-location="body" data-ga-name="leadership dib council" href="https://about.gitlab.com/company/culture/inclusion/leadership-dib-council/">Leadership DIB Council</a> consists of 13 Director+ leaders representing every division across all regions. Their task is to assist the DIB Team in implementing the corporate DIB strategy and provide insights to the DIB Team on the division's strategic initiatives. More specifically, the Leadership DIB Council is responsible for:
                - Aligning DIB initiatives with business goals
                - Providing division and/or geographic insights to the DIB Team
                - Representing DIB in the Executive Group
                - Actively advocating, challenging norms, and iterating to ensure that DIB remains top of mind at GitLab

        quote:
          text: |
            As a company that values transparency, publishing our 2023 Diversity, Inclusion, and Belonging report is an exciting milestone that highlights both our progress and areas to further build a more representative and equitable GitLab. Being the new executive sponsor of our Caregivers Team Member Resource Group, I'm excited to share my experience with other caregivers at GitLab and ensure we are exploring the best ways to meet the needs of our team members who are caring for others.”
          author: Wendy Nice Barnes
          role: GitLab Chief People Officer
          headshot:
            src: '/nuxt-images/dib/wendy-nice-barnes.png'
            alt: ''
  dib_marketplace:
    header: The future of Diversity, Inclusion, and Belonging
    sub_header: DIB helps us accelerate innovation, attract top talent, and deepen team member engagement. And we can't make everyone a contributor if we aren't connecting with every community. So it goes to say that DIB is an integral part of our future—not only as a people-centric practice but as a key contributor to our success in the DevSecOps industry.
    text: |
      So what does success look like? Of course, it includes continued improvement of our underrepresented group representation data, at the senior levels of the organization, beyond the current marks. It includes continuing to create new ways to make every team member feel comfortable to participate, empowered to excel, and open to express their authentic selves.
    read_more:
      text: |
        But DIB success also takes the form of increased market share, skyrocketing innovation, and greater customer sentiment. As we mentioned before, including everyone only makes GitLab better, and the effect we have on the market is a direct reflection of our focus on DIB.
      button_text: Read More
      button_text_open: Read Less
    quote:
      text: |
        DIB has significantly evolved throughout GitLab's history and I am encouraged by the support for TMRGs, for example, the Women's TMRG has grown from 19 members to 246 members today. As DIB evolves, there's still a need for more women in senior leadership and I'd like to see DIB be a part of every E-Group-level KPI and OKR.”
      author: Kyla Gradin-Dahl
      role: Area Sales Manager, U.S. West
      headshot:
        src: '/nuxt-images/dib/kyla-gradin-dahl.png'
        alt: ''
    how_accordion:
      header: How we continue to boost DIB at GitLab
      data_inbound_analytics: pricing-leap-target
      background: true
      groups:
        - header: "How we continue to boost DIB at GitLab"
          questions:
            - question: Enhancing corporate sustainability
              answer: >-
                Environmental Social Governance (ESG) practices, although newly formalized in GitLab's strategy, have been embedded in our work culture since its inception. Deeply integrated into the business philosophy, GitLab's ESG strategy is driven by our values of Collaboration, Results, Efficiency, Diversity, Inclusion and Belonging, Iteration, and Transparency (CREDIT).


                The ESG Team created and continuously maintains GitLab's corporate sustainability strategy and programs. This includes creating and managing ESG disclosures and public ESG reports, identifying and prioritizing key issues to advance GitLab's social and environmental goals, and creating partnerships with nonprofit organizations that support GitLab's values and mission. <a data-ga-location="body" data-ga-name="read gitlab's fy23 esg report for more information." href="https://about.gitlab.com/environmental-social-governance/">Read GitLab's FY23 ESG report for more information.</a>
            - question: Partnerships
              answer: >-
                We will continue to focus on strategic partnerships, content, and engagement platforms, focused on:

                  - DIB-first brand presence to increase brand recognition amongst underrepresented groups
                  - Providing increased opportunities to GitLab team members to engage with external communities focused on supporting underrepresented groups
                  - Increasing candidate engagement and attraction amongst underrepresented groups
            - question: Continuing to evolve our approach to talent acquisition
              answer: |
                While FY23 was rooted in building the foundational elements and introducing aspirational goals in Talent Acquisition, the year ahead will be highlighted by embedding inclusive hiring into our metrics, definitions of success, and best practices. Instead of adding “diversity” as a layer or lens to see Talent Acquisition (TA) through, we are setting out to make DIB a foundational ingredient in everything that we do. Through a continued partnership with the DIB, People Business Partners, and Data Analytics teams, we are focusing specifically on casting a wide net across multiple sources to increase representation at the candidate pipeline stage. While we establish a strong pipeline and evolve our best practices, we are also strategically planning other areas of outsized impact for the business and maturing our data to ensure we can make data-driven decisions through this work. Some specific areas of focus will include:

                - Partner on establishing strategic company-level OKRs
                - Launch our vision, mission, and 3-year TA strategy with inclusive hiring practices integrated throughout
                - Focus pipeline building across a wide range of sources to ensure talent attraction efforts reach underrepresented groups
            - question: Examining where we fall short
              answer: >-
                In the coming years, we will break down our populations into tech and non-tech to ensure we are further examining where we may have gaps.
            - question: Continued focus on underrepresented group representation
              answer: >-
                We will continue to increase underrepresented group representation across all job grades, aiming to exceed our CTO and CEO aspirational quarterly goals focused on URG management and senior leadership.
            - question: Focusing on retaining team members
              answer: >-
                We will continue to decrease regrettable underrepresented group (URG) attrition.
            - question: Managerial development
              answer: >-
                At GitLab we realize our managers are a critical population so we launched Elevate, our leadership development course, to upskill managers and embed inclusive behaviors aligned to our DIB value into each module. In addition to Elevate, we have a goal that our managers take a separate neurodiversity training and have exceeded our initial goal of 30% completions in FY23. We are striving to have all of our managers complete the training in FY24.
            - question: Creating a more equitable world beyond GitLab
              answer: >-
                In September 2022, the GitLab Foundation launched with a mission to improve people's lifetime earnings through access to opportunities, and a vision of a world in which one million more people can afford a better life. The GitLab Foundation was formed by GitLab Inc. as part of its mission to create a world in which everyone can contribute. GitLab leadership believed it was important to support organizations that could further this goal on a global scale. When GitLab Inc. went public in October 2021, it dedicated 1% of its shares to further this aim.

                The GitLab Foundation is currently funded by GitLab Inc. and its CEO, Sid Sijbrandij. The GitLab Foundation is an independent nonprofit entity and its operations are autonomous from GitLab Inc.

                To learn more, visit the <a data-ga-location="body" data-ga-name="gitlab foundation" href="https://www.gitlabfoundation.org/">GitLab Foundation.</a>
    leader_quote:
      text: |
        The GitLab Foundation is accelerating ways for people to gain the skills and opportunities needed to increase their lifetime earnings. Our vision is a world in which one million more people can afford a better life. As people thrive, so will their communities and economies.”
      author: Ellie Bertani
      role: President & CEO, GitLab Foundation
      headshot:
        src: '/nuxt-images/dib/ellie-bertani.png'
        alt: ''
  dib_conclusion:
    header: Our steadfast commitment to DIB
    text: |
      As we work toward achieving our mission of making it so that everyone can contribute, DIB will remain a key focus for GitLab. Over the last year we faced a number of external headwinds that impacted our global workforce in different ways. We dealt with an economic downturn, the fallout from Russia's invasion of Ukraine and the resulting war, the uncertainty of COVID, the uncertainty of political movements across the globe, and the reality that these factors weigh heavily on our team members.

      With DIB as one of our core values, we've been able to make progress in spite of these challenges and are more committed than ever to ensuring GitLab creates a workplace and community where everyone can contribute and everyone is welcome.
