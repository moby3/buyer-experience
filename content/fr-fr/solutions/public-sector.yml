---
  title: GitLab pour le secteur public
  description: Le codage social, l'intégration continue et l'automatisation des versions ont permis d'accélérer le développement et la qualité des logiciels afin d'atteindre les objectifs de la mission. En savoir plus !
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab pour le secteur public
        subtitle: La plateforme DevSecOps pour accélérer votre vitesse de mission
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Commencer votre essai gratuit
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Des questions ? Nous contacter
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/public-sector.jpg
          image_url_mobile: /nuxt-images/solutions/public-sector.jpg
          alt: "Image\_: gitlab pour le secteur public"
          bordered: true
    - name: 'by-industry-intro'
      data:
        logos:
          - name: Logo de l'Université de Washington
            image: /nuxt-images/logos/uw-logo.svg
            url: https://about.gitlab.com/customers/uw/
            aria_label: Link to University of Washington customer case study
          - name: Lockheed Martin Logo
            image: /nuxt-images/logos/lockheed-martin.png
            url: https://about.gitlab.com/customers/lockheed-martin/
            aria_label: Link to Lockheed Martin customer case study
          - name: Logo du comté de Cook
            image: /nuxt-images/logos/cookcounty-logo.svg
            url: https://about.gitlab.com/customers/cook-county/
            aria_label: Link to Cook County customer case study
          - name: University of Surrey Logo
            image: /nuxt-images/case-study-logos/logo_uni_surrey.svg
            url: https://about.gitlab.com/customers/university-of-surrey/
            aria_label: Link to University of Surrey customer case study
          - name: EAB Logo
            image: /nuxt-images/logos/eab-logo.svg
            url: https://about.gitlab.com/customers/EAB/
            aria_label: Link to E.A.B. customer case study
          - name: Victoria University Wellington Logo
            image: /nuxt-images/logos/victoria-university-wellington-logo.svg
            url: https://about.gitlab.com/customers/victoria_university/
            aria_label: Link to Victoria University Wellington customer case study
    - name: 'side-navigation-variant'
      links:
        - title: Présentation
          href: '#overview'
        - title: Témoignages
          href: '#testimonials'
        - title: Capacités
          href: '#capabilities'
        - title: Avantages
          href: '#benefits'
        - title: Études de cas
          href: '#case-studies'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Sécurité. Efficacité. Contrôle.
                image:
                  image_url: "/nuxt-images/solutions/benefits/by-solution-benefits-public-sector.jpeg"
                  alt: Image de collaboration
                is_accordion: true
                items:
                  - icon:
                      name: devsecops
                      alt: Icône Devsecops
                      variant: marketing
                    header: Réduire les risques liés à la sécurité et à la conformité
                    text: Découvrez les failles de sécurité et de conformité dès le début du processus tout en appliquant des garde-fous cohérents tout au long du cycle de vie DevSecOps.
                    link_text: En savoir plus sur le processus DevSecOps
                    link_url: /solutions/security-compliance/
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: repo-code
                      alt: Icône du code d'un dépôt
                      variant: marketing
                    header: Libérer des ressources
                    text: Offrez une meilleure expérience aux citoyens et contenez les menaces mondiales en éliminant les chaînes d'outils bricolées, fragiles et complexes qui entravent la collaboration et l'innovation.
                    link_text: Pourquoi choisir GitLab
                    link_url: /why-gitlab/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: digital-transformation
                      alt: Icône de transformation numérique
                      variant: marketing
                    header: Moderniser le processus tout en le simplifiant
                    text: Utilisez la plateforme DevSecOps conçue pour répondre aux besoins uniques des applications cloud-natives et de l'infrastructure sur laquelle elles s'appuient.
                    link_text: En savoir plus sur notre approche de plateforme
                    link_url: /solutions/devops-platform/
        - name: 'div'
          id: 'testimonials'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                header: |
                  Approuvée par les gouvernements.
                  <br />
                  Appréciée des développeurs.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/usarmy.svg
                      alt: Armée américaine
                    quote: Au lieu de devoir former les collaborateurs sur 10 outils différents, ces derniers n'apprennent à se servir que d'une seule plateforme, ce qui rend les choses beaucoup plus faciles à long terme.
                    author: Chris Apsey
                    position: Capitaine, armée américaine
                    ga_carousel: public-sector us army
                  - title_img:
                      url: /nuxt-images/logos/uw-logo.svg
                      alt: Logo de l'Université de Washington
                    quote: Au cours des deux dernières années, GitLab a transformé notre organisation à l'Université de Washington. Votre plateforme est fantastique !
                    author: Aaron Timss
                    position: Director of Information, CSE
                    ga_carousel: public-sector university of washington
                  - title_img:
                      url: /nuxt-images/logos/cookcounty-logo.svg
                      alt: Logo du comté de Cook
                    quote: Avec GitLab, je peux produire des recherches et les montrer à mes collègues de bureau. La plateforme GitLab me suggère des modifications à apporter à la recherche et je peux les effectuer sans avoir à me soucier du contrôle des versions et à penser à enregistrer mon travail. Un seul dépôt me permet de me concentrer davantage sur mon travail proprement dit et moins sur les mécanismes de travail.
                    author: Robert Ross
                    position: Chief Data Officer
                    ga_carousel: public-sector cook county
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: La plateforme DevSecOps la plus complète pour le secteur public
                sub_description: 'En commençant par la plateforme DevSecOps qui comprend la gestion sécurisée et robuste du code source (SCM), l''intégration continue (CI), la livraison continue (CD), ainsi que la sécurité et la conformité continues des logiciels, GitLab répond à vos besoins uniques, notamment :'
                white_bg: true
                sub_image: /nuxt-images/solutions/public-sector/showcase-pubsec.svg
                alt: image des avantages
                solutions:
                  - title: SBOM
                    description: Examinez la nomenclature logicielle de votre projet avec des détails clés sur les dépendances utilisées, notamment leurs vulnérabilités connues.
                    icon:
                      name: less-risk
                      alt: Icône Moins de risque
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: sbom
                    data_ga_location: solutions block
                  - title: Zero Trust
                    description: Découvrez comment GitLab suit les principes du modèle Zero Trust et démontre les meilleures pratiques.
                    icon:
                      name: monitor-pipeline
                      alt: Icône Pipeline de surveillance
                      variant: marketing
                    link_text: En savoir plus
                    link_url: /handbook/security/
                    data_ga_name: zero trust
                    data_ga_location: solutions block
                  - title: Gestion des vulnérabilités
                    description: "Gérez les vulnérabilités de vos logiciels en un seul endroit : dans le pipeline, pour le projet, les groupes de projets et entre plusieurs de vos groupes."
                    icon:
                      name: shield-check
                      alt: Icône de bouclier avec une coche
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/user/application_security/vulnerability_report/
                    data_ga_name: vulnerability management
                    data_ga_location: solutions block
                  - title: Tests à données aléatoires
                    description: GitLab vous permet d'ajouter des tests à données aléatoires à vos pipelines, ainsi qu'un ensemble complet de scanners. Les tests à données aléatoires envoient des entrées aléatoires à une version instrumentée de votre application afin de provoquer un comportement inattendu. Ce comportement indique des failles de sécurité et de logique qui doivent être corrigées.
                    icon:
                      name: monitor-test
                      alt: Icône de test de surveillance
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/
                    data_ga_name: fuzz testing
                    data_ga_location: solutions block
                  - title: Environnements hors connexion
                    description: Même si vous n'avez pas de connexion à Internet active, vous pouvez exécuter la plupart des scanners de sécurité de GitLab.
                    icon:
                      name: gitlab-monitor-alt
                      alt: Icône d'un écran avec le logo GitLab
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/user/application_security/offline_deployments/
                    data_ga_name: offline environment
                    data_ga_location: solutions block
                  - title: Contrôles courants en matière de conformité
                    description: Automatisez et appliquez des politiques courantes, telles que la séparation des tâches, la protection des branches et les règles « push ».
                    icon:
                      name: shield-check
                      alt: Icône de bouclier avec une coche
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: common controls for compliance
                    data_ga_location: solutions block
                  - title: Pipelines de conformité
                    description: Appliquez les configurations d'analyse des pipelines pour vous assurer que les analyses de sécurité requises ne sont pas contournées.
                    icon:
                      name: shield-check
                      alt: Icône de bouclier avec une coche
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: compliance
                    data_ga_location: solutions block
                  - title: Développement du bas vers le haut
                    description: Faites en sorte que vos équipes de développement puissent collaborer, aussi variées soient-elles.
                    icon:
                      name: cog-code
                      alt: Icône de code dans un rouage
                      variant: marketing
                    link_text: En savoir plus
                    link_url: https://www.youtube.com/watch?v=HSfDTslLRT8/
                    data_ga_name: low to high
                    data_ga_location: solutions block
                  - title: Sur site, auto-hébergé ou SaaS
                    description: GitLab fonctionne dans tous les environnements. Le choix vous appartient.
                    icon:
                      name: gitlab-monitor-alt
                      alt: Icône d'un écran avec le logo GitLab
                      variant: marketing
                    link_text: En savoir plus
                    link_url: /pricing/
                    data_ga_name: on-prem self-hosted or saas
                    data_ga_location: solutions block
                  - title: Image de conteneur renforcée
                    description: L'image de conteneur renforcée conforme au DoD minimise le profil de risque, permet de déployer rapidement des applications plus sécurisées et prend en charge l'autorisation continue d'exploiter les processus ; cette procédure est également acceptée dans les conteneurs Iron Bank.
                    icon:
                      name: lock-cog
                      alt: Icône de rouage au centre d'un cadenas
                      variant: marketing
                    link_text: En savoir plus
                    link_url: /press/releases/2020-07-01-gitlab-announces-hardened-container-image-in-support-of-the-us-department-of-defense-enterprise-devsecops-initiative.html
                    data_ga_name: hardened container image
                    data_ga_location: solutions block
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: Convient particulièrement au secteur public
                cards:
                  - title: NIST SSDF
                    description: GitLab est aligné sur les directives du NIST, qui aident les DSI à mettre en œuvre les actions requises en matière de sécurité de la chaîne d'approvisionnement des logiciels, afin de défendre leurs agences de manière proactive. En savoir plus sur la façon dont <a href="/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/" data-ga-name="gitlab meets nist" data-ga-location="value prop">GitLab répond aux directives NIST SSDF 1.1</a>.
                    icon:
                      name: less-risk
                      alt: Icône Moins de risque
                      variant: marketing
                  - title: L'alternative DI2E
                    description: L'accès à DI2E (Defense Intelligence Information Enterprise) a été annulé, obligeant les agences à repenser l'ensemble de leur modèle DevSecOps. GitLab est une alternative solide à DI2E, et notre application tout-en-un simplifie l'approvisionnement.
                    icon:
                      name: devsecops
                      alt: Icône DevSecOps
                      variant: marketing
                  - title: Visibilité et contrôle de la chaîne d'approvisionnement
                    description: La plateforme DevSecOps de GitLab se présente sous la forme d'une application tout-en-un et <a href="/press/releases/2020-07-01-gitlab-announces-hardened-container-image-in-support-of-the-us-department-of-defense-enterprise-devsecops-initiative.html" data-ga-name="hardened" data-ga-location="value prop">renforcée</a> qui simplifie la visibilité et la traçabilité de bout en bout. Les politiques de sécurité et de conformité sont gérées et appliquées de manière cohérente sur l'ensemble de vos processus DevSecOps.
                    icon:
                      name: eye-magnifying-glass
                      alt: Icône de loupe
                      variant: marketing
                  - title: Sur site, auto-hébergé ou SaaS
                    description: Le choix vous appartient.
                    icon:
                      name: monitor-web-app
                      alt: Icône d'application Web sur un écran
                      variant: marketing
        - name: 'div'
          id: 'case-studies'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-case-studies'
              data:
                title: Témoignages de clients
                link:
                  text: Toutes les études de cas
                rows:
                  - title: U.S. Army Cyber School
                    subtitle: Comment la U.S. Army Cyber School a créé « Courseware as Code » avec GitLab
                    image:
                      url: /nuxt-images/blogimages/us-army-cyber-school.jpeg
                      alt: Soldat s'exprimant à la radio
                    button:
                      href: /customers/us_army_cyber_school/
                      text: En savoir plus
                      data_ga_name: us army learn more
                      data_ga_location: case studies
                  - title: Bureau de l'évaluateur du comté de Cook
                    subtitle: Le comté de Cook, à Chicago, évalue les données économiques en toute transparence ainsi que le contrôle de version
                    image:
                      url: /nuxt-images/blogimages/cookcounty.jpg
                      alt: Maisons vues d'en haut
                    button:
                      href: /customers/cook-county/
                      text: En savoir plus
                      data_ga_name: cook country learn more
                      data_ga_location: case studies
                  - title: Université de Washington
                    subtitle: Le Paul G. Allen Center for Computer Science & Engineering gagne en contrôle et en flexibilité pour gérer facilement plus de 10 000 projets.
                    image:
                      url: /nuxt-images/blogimages/uw-case-study-image.png
                      alt: Campus de l'Université de Washington
                    button:
                      href: /customers/uw/
                      text: En savoir plus
                      data_ga_name: uw learn more
                      data_ga_location: case studies
    - name: 'by-solution-link'
      data:
        title: 'Événements GitLab'
        description: "Participez à un événement pour découvrir comment votre équipe peut livrer des logiciels plus rapidement et plus efficacement, tout en renforçant la sécurité et la conformité. Nous participerons, hébergerons et organiserons de nombreux événements en 2022, et nous avons hâte de vous y rencontrer\_!"
        link: /events/
        button_text: En savoir plus
        image: /nuxt-images/events/google-cloud-next/pubsec-event-link.jpeg
        alt: foule lors d'une présentation
        icon: calendar-alt-2
