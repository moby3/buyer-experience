---
  title: What is a continuous integration server?
  description: Learn more about the role of a continuous integration server in software development.
  date_published: 2023-05-03
  date_modified: 2023-05-03
  topics_header:
    data:
      title: What is a continuous integration server?
      block:
        - metadata:
            id_tag: what-is-a-continuous-integration-server
          text: |
            Continuous integration (CI) is a staple of modern software development. While CI doesn’t technically require specific tools, most teams use a continuous integration server to help them streamline processes.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: CI CD
      href: /topics/ci-cd/
      data-ga-name: ci-cd
      data_ga_location: breadcrumb
    - title: Continuous integration server
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: What does a CI server do?
          href: "#what-does-a-ci-server-do"
          data_ga_name: what does a ci server do
          data_ga_location: side-navigation
        - text: CI server vs. manual builds
          href: "#ci-server-vs-manual-builds"
          data_ga_name: ci servers vs manual builds
          data_ga_location: side-navigation
        - text: Advantages of a CI server
          href: "#advantages-of-a-ci-server"
          data_ga_name: advantages of a ci server
          data_ga_location: side-navigation
        - text: Examples of CI servers
          href: "#examples-of-ci-servers"
          data_ga_name: examples of ci servers
          data_ga_location: side-navigation
    content:
      - name: topics-copy-block
        data:
          header: What does a CI server do?
          column_size: 10
          blocks:
            - text: |
                A [continuous integration](/topics/ci-cd/){data-ga-name="continuous-integration" data-ga-location="body"} server (sometimes known as a build server) essentially manages the shared repository and acts as a referee for the code coming in. When developers commit to the repository, the CI server [initiates a build](https://www.martinfowler.com/articles/continuousIntegration.html) and documents the results of the build. Typically, the developer that committed the code change to the repository will receive an email notification with the results.

                The majority of teams building software today are practicing continuous integration. Of those teams practicing continuous integration, most rely on a CI server to automate builds.
      - name: topics-copy-block
        data:
          header: CI server vs. manual builds
          column_size: 10
          blocks:
            - text: |
                For teams that do not use a CI server, they’re still able to achieve continuous integration through periodic builds they manage internally. Teams can use scripts they build themselves or manually trigger builds. Continuous integration isn’t a tool in iself, it’s a larger framework with a set of practices aided by certain tools.

                For teams that want to practice continuous integration, there are some [helpful tools](/topics/ci-cd/implement-continuous-integration/#essential-continuous-integration-tools){data-ga-name="continuous integration tools" data-ga-location="body"} that can make continuous integration easier. For teams that bypass using a CI server, it’s about having more control over the source code, testing, and commit processes.
      - name: topics-copy-block
        data:
          header: Advantages of a CI server
          column_size: 10
          blocks:
            - text: |
                Teams find CI servers useful in software development because it can offer certain process advantages.

                ### Automated tests

                When code is committed using a CI server, errors in the code are detected automatically. The server tests the code and provides feedback to the committer quickly, without the committer having to initiate a manual build.

                ### Better collaboration

                Instead of relying on developers to keep the shared repository up to date, the CI server manages all code coming in and maintains the integrity of the source code through automatic builds and tests. This allows developers to focus on their own projects instead of worrying about other projects breaking their tests. Teams can collaborate without worrying about code.

                ### Streamlined workflow

                Without CI servers, developers are working on different layers of the application with code saved on local machines. While teams that do manual or self-managed testing can get around these potential issues, a CI server takes this extra layer of coordination out of the workflow.
      - name: topics-copy-block
        data:
          header: Examples of CI servers
          column_size: 10
          blocks:
            - text: |
                The majority of tools we consider continuous integration tools are, in fact, CI servers. Some include additional functionality, such as source code management, [continuous delivery](/stages-devops-lifecycle/continuous-delivery/){data-ga-name="continuous delivery" data-ga-location="body"}, and testing.

                ### GitLab CI

                Learn more about [GitLab CI](/solutions/continuous-integration/){data-ga-name="gitlab ci" data-ga-location="body"}

                ### Jenkins

                [GitLab CI vs Jenkins](/devops-tools/jenkins-vs-gitlab/){data-ga-name="gitlab ci vs jenkins" data-ga-location="body"}

                ### CircleCI

                [GitLab CI vs CircleCI](/competition/circleci/){data-ga-name="gitlab ci vs circle ci" data-ga-location="body"}
  components:
    - name: solutions-resource-cards
      data:
        title: More on continuous integration
        column_size: 6
        cards:
          - icon:
              name: ebook
              variant: marketing
              alt: Ebook Icon
            event_type: "Book"
            header: How to convince leadership to adopt CI/CD
            image: "/nuxt-images/resources/resources_5.jpeg"
            link_text: "Learn more"
            href: https://page.gitlab.com/2021_eBook_leadershipCICD.html?_gl=1*1fs83aa*_ga*MTEyNzY5NDEzNy4xNjY4MDkzODgx*_ga_ENFH3X7M5Y*MTY4MjcxNjc3Ny4yMDIuMS4xNjgyNzE2Nzg1LjAuMC4w
            data_ga_name: how to convince leadership to adopt ci cd
            data_ga_location: body
    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blog"
            header: "Getting [meta] with GitLab CI/CD: Building build images"
            text: |
              Let's talk about building build images with GitLab CI/CD. The power of Docker as a build platform is unleashed when you get meta.
            link_text: "Learn more"
            href: /blog/2019/08/28/building-build-images/
            image: /nuxt-images/blogimages/autodevops.jpg
            data_ga_name: "Getting [meta] with GitLab CI/CD: Building build images"
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blog"
            header: "A beginner's guide to continuous integration"
            text: |
              Here's how to help everyone on your team, like designers and testers, get started with GitLab CI.
            link_text: "Learn more"
            href: /blog/2018/01/22/a-beginners-guide-to-continuous-integration/
            image: /nuxt-images/blogimages/beginners-guide-to-ci.jpg
            data_ga_name: "A beginner's guide to continuous integration"
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blog"
            header: "Need DevOps buy-in? Here's how to convince stakeholders"
            text: |
              If you need to make the case for DevOps to a non-technical crowd, it's important to be prepared. Here's what you need to know.
            link_text: "Learn more"
            href: /blog/2020/09/24/devops-stakeholder-buyin/
            image: /nuxt-images/blogimages/bi_worldwise_casestudy_image.png
            data_ga_name: "Need DevOps buy-in? Here's how to convince stakeholders"
            data_ga_location: resource cards
