---
  title: InnerSource with GitLab
  description: InnerSource is the use of open source best practices and establishment of an open source culture within your organization. Find out more!
  template: 'industry'
  no_gradient: true
  nextstep_variant: 5
  components:
    - name: 'solutions-hero'
      data:
        title: InnerSource with GitLab
        subtitle: Integrated Developer Portal to harness open source best practices and processes to work and collaborate more effectively.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Start your free trial
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/stock3.png
          image_url_mobile: /nuxt-images/solutions/stock3.png
          alt: "Image: gitlab for public sector"
          bordered: true
    - name: 'side-navigation-variant'
      links:
        - title: Benefits
          href: '#benefits'
        - title: Capabilities
          href: '#capabilities'
        - title: Pricing
          href: '#pricing'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Why InnerSource?
                subtitle: |
                  High-performing development and engineering teams are adopting open source processes and culture to improve the developer experience and improve collaboration. Teams are using GitLab as an internal development portal to:
                link:
                    text: Learn more about InnerSource
                    href: https://about.gitlab.com/topics/version-control/what-is-innersource/
                    ga_name: innersource topic
                    ga_location: benefits
                is_accordion: true
                items:
                  - icon:
                      name: speed-alt-2
                      alt: speed-alt-2 Icon
                      variant: marketing
                    header: Build better software, faster
                    text: Unit tests, code coverage, continuous integration and built-in end-to-end security help strengthen and improve code earlier.
                  - icon:
                      name: docs-alt
                      alt: Docs Icon
                      variant: marketing
                    header: Create comprehensive documentation
                    text: Code is better documented in MRs using comments and suggested changes and in wikis, issues and READMEs for process
                  - icon:
                      name: stopwatch
                      alt: stopwatch Icon
                      variant: marketing
                    header: Achieve higher efficiencies
                    text: Code, architecture, and assets are discoverable and available across teams and the organization
                  - icon:
                      name: user-collaboration
                      alt: user-collaboration Icon
                      variant: marketing
                    header: Improve Collaboration
                    text: Friction reduced for code review, communication, and contributions in and between teams across the organization, in a single platform.
                  - icon:
                      name: user-laptop
                      alt: user-laptop Icon
                      variant: marketing
                    header: Enhance team member experience
                    text: Silos broken down, productivity and happiness improved and with that, better team retainment and recruitment         
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'solutions-by-solution-list'
              data:
                title: InnerSource with GitLab
                icon: slp-check-circle-alt
                header: "GitLab enables software development teams to improve quality, gain efficiencies, increase collaboration and accelerate innovation, by delivering a streamlined and delightful developer experience.  This experience is optimized by GitLab’s single DevSecOps platform approach with an integrated Internal Developer Portal featuring:"
                items: 
                  - Process and product documentation in customizable READMEs and runbooks
                  - Centralized, collaborative code reviews with [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/){data-ga-name="merge request" data-ga-location="capabilities"}
                  - Knowledge management through  [Wikis](https://docs.gitlab.com/ee/user/project/wiki/){data-ga-name="wikis" data-ga-location="capabilities"}  
                  - Ideation, communication, and updates via [issues](https://docs.gitlab.com/ee/user/project/issues){data-ga-name="issues" data-ga-location="capabilities"} and [threaded discussions](https://docs.gitlab.com/ee/user/discussions/){data-ga-name="threaded discussions" data-ga-location="capabilities"}
                  - Comprehensive API documentation
                  - Visualization of test coverage results
                  - Powerful Web IDE based on VS Code, housed directly in the UI
                  - Native CLI 
                footer: All housed in the same platform where teams work from planning to production.
                cta:
                  text: Learn how to unlock InnerSource
                  video: https://www.youtube.com/embed/ZS1mCpBHXaI
                  ga_name: innersource unlock
                  ga_location: capabilities
                  
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: Which plan is right for you?
                cta:
                  url: /pricing/
                  text: Learn more about pricing
                  data_ga_name: pricing learn more
                  data_ga_location: pricing
                  aria_label: pricing
                tiers:
                  - id: free
                    title: Free
                    items:
                      - SAST and Secrets Detection
                      - Findings in json file
                  - id: premium
                    title: Premium
                    items:
                      - Everything in Free plus
                      - Approvals and guardrails
                    link:
                      href: /pricing/premium/
                      text: Learn about Premium
                      data_ga_name: premium learn more
                      data_ga_location: pricing
                      aria_label: premium tier
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Everything in Premium plus
                      - DAST, API Fuzzing, License Compliance, and more
                      - SBOM and Dependency lists
                      - Vulnerability management
                      - Compliance management
                    link:
                      href: /pricing/ultimate
                      text: Learn about Ultimate
                      data_ga_name: ultimate learn more
                      data_ga_location: pricing
                      aria_label: ultimate tier
                    cta:
                      href: /free-trial/
                      text: Try Ultimate for Free
                      data_ga_name: try ultimate
                      data_ga_location: pricing
                      aria_label: ultimate tier
      